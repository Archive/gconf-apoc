AC_DEFUN([AC_FYI], [echo "FYI: " $1])

AC_INIT(src/apoc-backend.c)

AM_INIT_AUTOMAKE(apoc-adapter-gconf, 1.1.0)

AM_MAINTAINER_MODE

AC_PROG_CC
AC_PROG_CXX
AC_ISC_POSIX
AM_PROG_CC_STDC
AC_HEADER_STDC
AM_PROG_LIBTOOL

changequote(,)dnl
if test "x$GCC" = "xyes"; then
  case " $CFLAGS " in
  *[\ \	]-Wall[\ \	]*) ;;
  *) CFLAGS="$CFLAGS -Wall" ;;
  esac

  if test "x$enable_ansi" = "xyes"; then
    case " $CFLAGS " in
    *[\ \	]-ansi[\ \	]*) ;;
    *) CFLAGS="$CFLAGS -ansi" ;;
    esac

    case " $CFLAGS " in
    *[\ \	]-pedantic[\ \	]*) ;;
    *) CFLAGS="$CFLAGS -pedantic" ;;
    esac
  fi
fi
changequote([,])dnl

dnl used to rename everything and support simultaneous installs.
dnl not incremented for bugfix or unstable releases.
MAJOR_VERSION=0
AC_SUBST(MAJOR_VERSION)

GETTEXT_PACKAGE=apoc-adapter-gconf$MAJOR_VERSION
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE", [Gettext package])

dnl libtool versioning for libgconf

dnl increment if the interface has additions, changes, removals.
APOC_ADAPTER_GCONF_CURRENT=0

dnl increment any time the source changes; set to 
dnl  0 if you increment CURRENT
APOC_ADAPTER_GCONF_REVISION=1

dnl increment if any interfaces have been added; set to 0
dnl  if any interfaces have been removed. removal has 
dnl  precedence over adding, so set to 0 if both happened.
APOC_ADAPTER_GCONF_AGE=0

AC_SUBST(APOC_ADAPTER_GCONF_CURRENT)
AC_SUBST(APOC_ADAPTER_GCONF_REVISION)
AC_SUBST(APOC_ADAPTER_GCONF_AGE)

dnl Version of GConf being dealt with.
GCONF_MAJOR_VERSION=2

AC_SUBST(GCONF_MAJOR_VERSION)

# find the actual value for $prefix that we'll end up with
REAL_PREFIX=
if test "x$prefix" = "xNONE"; then
  REAL_PREFIX=$ac_default_prefix
else
  REAL_PREFIX=$prefix
fi

# Have to go $sysconfdir->$prefix/etc->/usr/local/etc   
# if you actually know how to code shell then fix this :-)
SYSCONFDIR_TMP="$sysconfdir"
old_prefix=$prefix
prefix=$REAL_PREFIX
EXPANDED_SYSCONFDIR=`eval echo $SYSCONFDIR_TMP`
prefix=$old_prefix
AC_SUBST(EXPANDED_SYSCONFDIR)

dnl Save flags to aclocal
ACLOCAL="$ACLOCAL $ACLOCAL_FLAGS"

dnl Default to debug spew in unstable branch
AC_ARG_ENABLE(debug, 
            [  --enable-debug=[no/yes/minimum]      Compile with debug checks.],,enable_debug=minimum)

if test "x$enable_debug" = "xyes"; then
        CFLAGS="$CFLAGS"
        AC_FYI("Will build with debugging spew and checks")
else
  if test "x$enable_debug" = "xno"; then
          CFLAGS="$CFLAGS -DG_DISABLE_CHECKS=1 -DG_DISABLE_ASSERT=1"
          AC_FYI("Will build without *any* debugging code")
  else
          AC_FYI("Will build with debug checks but no debug spew")
  fi
fi

AC_ARG_WITH(html-dir, [  --with-html-dir=PATH path to installed docs ])

if test "x$with_html_dir" = "x" ; then
  HTML_DIR='${datadir}/gtk-doc/html'
else
  HTML_DIR=$with_html_dir
fi

AC_SUBST(HTML_DIR)


AC_CHECK_PROG(GTKDOC, gtkdoc-mkdb, true, false)

gtk_doc_min_version=0.6
if $GTKDOC ; then 
    gtk_doc_version=`gtkdoc-mkdb --version`
    AC_MSG_CHECKING([gtk-doc version ($gtk_doc_version) >= $gtk_doc_min_version])

    IFS="${IFS= 	}"; gconf_save_IFS="$IFS"; IFS="."
    set $gtk_doc_version
    for min in $gtk_doc_min_version ; do
        cur=$1; shift
        if test -z $min ; then break; fi
        if test -z $cur ; then GTKDOC=false; break; fi
        if test $cur -gt $min ; then break ; fi
        if test $cur -lt $min ; then GTKDOC=false; break ; fi
    done
    IFS="$gconf_save_IFS"

    if $GTKDOC ; then
      AC_MSG_RESULT(yes)
    else
      AC_MSG_RESULT(no)
    fi
fi

AM_CONDITIONAL(HAVE_GTK_DOC, $GTKDOC)
AC_SUBST(HAVE_GTK_DOC)

AC_CHECK_PROG(DB2HTML, db2html, true, false)
AM_CONDITIONAL(HAVE_DOCBOOK, $DB2HTML)

dnl Let people disable the gtk-doc stuff.
AC_ARG_ENABLE(gtk-doc, [  --enable-gtk-doc  Use gtk-doc to build documentation [default=auto]], enable_gtk_doc="$enableval", enable_gtk_doc=auto)

if test x$enable_gtk_doc = xauto ; then
  if test x$GTKDOC = xtrue ; then
    enable_gtk_doc=yes
  else
    enable_gtk_doc=no 
  fi
fi

dnl NOTE: We need to use a separate automake conditional for this
dnl       to make this work with the tarballs.
AM_CONDITIONAL(ENABLE_GTK_DOC, test x$enable_gtk_doc = xyes)

PKGCONFIG_MODULES='gconf-2.0 >= 2.0.1 libxml-2.0'

PKG_CHECK_MODULES(DEPENDENT, $PKGCONFIG_MODULES)

AC_SUBST(DEPENDENT_LIBS)
AC_SUBST(DEPENDENT_CFLAGS)

AC_CHECK_HEADER(pthread.h, have_pthread=yes)
AM_CONDITIONAL(PTHREADS, test x$have_pthread=xyes)

ALL_LINGUAS="de es fr it ja ko pl pt pt_BR ru sv zh_CN zh_HK zh_TW"

AM_GLIB_GNU_GETTEXT

# AM_GNU_GETTEXT above substs $DATADIRNAME
# this is the directory where the *.{mo,gmo} files are installed
gconflocaledir='${prefix}/${DATADIRNAME}/locale'
AC_SUBST(gconflocaledir)

AC_CHECK_FUNCS(bind_textdomain_codeset)

AC_SUBST(CFLAGS)
AC_SUBST(CPPFLAGS)
AC_SUBST(LDFLAGS)

## Just for debugging purposes
absolute_top_srcdir=`pwd`
AC_SUBST(absolute_top_srcdir)

AC_OUTPUT([
patched.path
Makefile
src/Makefile
po/Makefile.in
])


