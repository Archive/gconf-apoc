/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either
 * the GNU General Public License Version 2 only ("GPL") or
 * the Common Development and Distribution License("CDDL")
 * (collectively, the "License"). You may not use this file
 * except in compliance with the License. You can obtain a copy
 * of the License at www.sun.com/CDDL or at COPYRIGHT. See the
 * License for the specific language governing permissions and
 * limitations under the License. When distributing the software,
 * include this License Header Notice in each file and include
 * the License file at /legal/license.txt. If applicable, add the
 * following below the License Header, with the fields enclosed
 * by brackets [] replaced by your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by
 * only the CDDL or only the GPL Version 2, indicate your
 * decision by adding "[Contributor] elects to include this
 * software in this distribution under the [CDDL or GPL
 * Version 2] license." If you don't indicate a single choice
 * of license, a recipient has the option to distribute your
 * version of this file under either the CDDL, the GPL Version
 * 2 or to extend the choice of license to its licensees as
 * provided above. However, if you add GPL Version 2 code and
 * therefore, elected the GPL Version 2 license, then the
 * option applies only if the new code is made subject to such
 * option by the copyright holder.
 */


#ifndef GConf_Backend_H
#define GConf_Backend_H

#include <gconf/gconf.h>
#include <gconf/gconf-listeners.h>

typedef struct privGConfBackend GConfBackend ;

/* A GConfSource definition.
   Lifted from gconf-sources.h in GConf. */
typedef struct 
{
    guint flags ;
    gchar *address ;
    GConfBackend *backend ;
} GConfSource ;

typedef enum
{
    GCONF_SOURCE_ALL_WRITEABLE = 1 << 0,
    GCONF_SOURCE_ALL_READABLE = 1 << 1,
    GCONF_SOURCE_NEVER_WRITEABLE = 1 << 2,
    GCONF_SOURCE_ALL_FLAGS = ((1 << 0) | (1 << 1))
} GConfSourceFlags ;

typedef void (*GConfSourceNotifyFunc)(GConfSource *source,
                                      const gchar *key,
                                      gpointer user_data) ;
/* The GConf backend VTable. 
   Lifted from the official gconf-backend.h in GConf. */
typedef struct 
{
    gsize vtable_size ;

    void (* shutdown)(GError** err) ;
    GConfSource *(* resolve_address)(const gchar* address, GError** err) ;
    
    void (* lock)(GConfSource* source, GError** err) ;
    void (* unlock)(GConfSource* source, GError** err) ;
    
    gboolean (* readable)(GConfSource* source, const gchar* key, GError** err) ;
    gboolean (* writable)(GConfSource* source, const gchar* key, GError** err) ;
    
    GConfValue *(* query_value)(GConfSource* source, const gchar* key, 
                                const gchar** locales, gchar** schema_name, 
                                GError** err) ;
    GConfMetaInfo *(* query_metainfo)(GConfSource* source, const gchar* key, 
                                      GError** err) ;
    void (* set_value)(GConfSource* source, const gchar* key, 
                       const GConfValue* value, GError** err) ;
    
    GSList *(* all_entries)(GConfSource* source, const gchar* dir, 
                            const gchar** locales, GError** err) ;
    GSList *(* all_subdirs)(GConfSource* source, const gchar* dir, 
                            GError** err) ;
    
    void (* unset_value)(GConfSource* source, const gchar* key, 
                         const gchar* locale, GError** err) ;
    gboolean (* dir_exists)(GConfSource* source, const gchar* dir, 
                            GError** err) ;
    void (* remove_dir)(GConfSource* source, const gchar* dir, GError** err) ;
    void (* set_schema)(GConfSource* source, const gchar* key, 
                        const gchar* schema_key, GError** err) ;
    
    gboolean (* sync_all)(GConfSource* source, GError** err) ;
    void (* destroy_source)(GConfSource* source) ;
    void (* clear_cache)(GConfSource* source) ;
    void (* blow_away_locks)(const gchar* address) ;
    
    void (*set_notify_func)(GConfSource * source, 
                            GConfSourceNotifyFunc notify_func,
                            gpointer user_data) ;
    void (*add_listener)(GConfSource* source, guint id, 
                         const gchar* namespace_section) ;
    void (*remove_listener)(GConfSource *source, guint id) ;
} GConfBackendVTable ;


/* Address parsing methods.
   Lifted from gconf-backend.h in GConf. */
gchar **gconf_address_flags(const gchar *aAddress) ;
gchar *gconf_address_resource(const gchar *aAddress) ;

#endif /* GConf_Backend_H */ 
