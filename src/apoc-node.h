/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either
 * the GNU General Public License Version 2 only ("GPL") or
 * the Common Development and Distribution License("CDDL")
 * (collectively, the "License"). You may not use this file
 * except in compliance with the License. You can obtain a copy
 * of the License at www.sun.com/CDDL or at COPYRIGHT. See the
 * License for the specific language governing permissions and
 * limitations under the License. When distributing the software,
 * include this License Header Notice in each file and include
 * the License file at /legal/license.txt. If applicable, add the
 * following below the License Header, with the fields enclosed
 * by brackets [] replaced by your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by
 * only the CDDL or only the GPL Version 2, indicate your
 * decision by adding "[Contributor] elects to include this
 * software in this distribution under the [CDDL or GPL
 * Version 2] license." If you don't indicate a single choice
 * of license, a recipient has the option to distribute your
 * version of this file under either the CDDL, the GPL Version
 * 2 or to extend the choice of license to its licensees as
 * provided above. However, if you add GPL Version 2 code and
 * therefore, elected the GPL Version 2 license, then the
 * option applies only if the new code is made subject to such
 * option by the copyright holder.
 */


#ifndef Apoc_Node_H
#define Apoc_Node_H

#include <gconf/gconf.h>

/**
  Structure and methods associated to a configuration node in the
  merged tree.
  */
typedef struct privApocNode ApocNode ;

/**
  Destructor.
  */
void destroyApocNode(ApocNode *aNode, gboolean aDestroyChildren) ;
/**
  Maps a node to a GConfValue object.
  */
GConfValue *apocNodeToGConfValue(const ApocNode *aNode,
                                 const gchar **aLocales,
                                 const ApocNode *aReplacements,
                                 GError **aError) ;
/**
  Returns the meta-information corresponding to a node.
  */
GConfMetaInfo *apocNodeToGConfMetaInfo(const ApocNode *aNode,
                                       GError **aError) ;
/**
  Returns the property nodes located under a node.
  */
GSList *apocNodeListProperties(const ApocNode *aNode,
                               GError **aError) ;
/**
  Returns the sub nodes located under a node.
  */
GSList *apocNodeListSubnodes(const ApocNode *aNode, GSList *aList) ;
/**
  Tells whether the node is finalised or not.
  */
gboolean apocNodeIsFinalised(const ApocNode *aNode) ;
/**
  Sets the finalised status of a node.
  */
void apocNodeSetFinalised(ApocNode *aNode, gboolean aIsFinalised) ;
/**
  Tells whether the node is a property node or not.
  */
gboolean apocNodeIsProperty(const ApocNode *aNode) ;
/**
  Returns a reference to the node name, relative to its parent.
  */
const gchar *apocNodeGetName(const ApocNode *aNode) ;
/**
  Returns an allocated string containing the full path of the node.
  */
gchar *apocNodeGetFullName(const ApocNode *aNode) ;
/**
  Definition of an action callback invoked when comparing nodes.
  */
typedef void (*ApocNodeCompareAction)(const ApocNode *aNode1,
                                      const ApocNode *aNode2,
                                      gpointer aUserData) ;
/**
  Compares nodes (and their subtree) and calls an action for each difference.
  */
void apocNodeCompare(const ApocNode *aNode1, const ApocNode *aNode2,
                     ApocNodeCompareAction aAction,
                     gpointer aUserData) ;

#endif /* Apoc_Node_H */
                                                                                

