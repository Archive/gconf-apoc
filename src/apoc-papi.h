/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either
 * the GNU General Public License Version 2 only ("GPL") or
 * the Common Development and Distribution License("CDDL")
 * (collectively, the "License"). You may not use this file
 * except in compliance with the License. You can obtain a copy
 * of the License at www.sun.com/CDDL or at COPYRIGHT. See the
 * License for the specific language governing permissions and
 * limitations under the License. When distributing the software,
 * include this License Header Notice in each file and include
 * the License file at /legal/license.txt. If applicable, add the
 * following below the License Header, with the fields enclosed
 * by brackets [] replaced by your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by
 * only the CDDL or only the GPL Version 2, indicate your
 * decision by adding "[Contributor] elects to include this
 * software in this distribution under the [CDDL or GPL
 * Version 2] license." If you don't indicate a single choice
 * of license, a recipient has the option to distribute your
 * version of this file under either the CDDL, the GPL Version
 * 2 or to extend the choice of license to its licensees as
 * provided above. However, if you add GPL Version 2 code and
 * therefore, elected the GPL Version 2 license, then the
 * option applies only if the new code is made subject to such
 * option by the copyright holder.
 */


#ifndef Apoc_Papi_H
#define Apoc_Papi_H

#include <glib.h>

/** 
  Stuff stolen from the PAPI definition, needs to be in sync
  with that API.
  */
typedef struct PAPI PAPI ;

typedef enum
{
    PAPISuccess = 0,
    PAPIConnectionFailure,
    PAPIAuthenticationFailure,
    PAPINoSuchComponentFailure,
    PAPILDBFailure,
    PAPIInvalidArg,
    PAPIOutOfMemoryFailure,
    PAPIUnknownFailure
} PAPIStatus ;

const gchar *apocPapiGetError(PAPIStatus aStatus) ;

typedef enum
{
    PAPIComponentAdd,
    PAPIComponentModify,
    PAPIComponentRemove
} PAPIEventType ;

typedef struct
{
    PAPIEventType mEventType ;
    const gchar *mComponent ;
} PAPIEvent ;

typedef void (*PAPIListener)(const PAPIEvent *aEvent, void *aUserData) ;

typedef void *PAPIListenerId ;

typedef enum 
{
    PAPIConnected,
    PAPIDisconnected
} PAPIConnectionState ;

typedef void (*PAPIConnectionListener)(PAPIConnectionState, void *) ;

typedef struct PAPILayerList
{
    const guchar *mData ;
    gint mSize ;
    const gchar *mTimeStamp ;
    struct PAPILayerList *mNext ;
} PAPILayerList ;

typedef struct PAPIStringList
{
    const gchar *mData ;
    struct PAPIStringList *mNext ;
} PAPIStringList ;

/** 
  Entry points of the PAPI. 
  The function signatures need to match the PAPI ones.
  */
typedef struct 
{
    PAPI *(*mInitialise)(const gchar *, PAPIStatus *) ;
    PAPI *(*mInitialiseWithListener)(const gchar *, 
                                     PAPIConnectionListener,
                                     void *,
                                     PAPIStatus *) ;
    void (*mDeinitialise)(PAPI *, PAPIStatus *) ;
    PAPILayerList *(*mReadComponentLayers)(const PAPI *, 
                                           const gchar *,
                                           PAPIStatus *) ;
    PAPIStringList *(*mListComponentNames)(const PAPI *, 
                                           const gchar *, 
                                           PAPIStatus *) ;
    void (*mSetOffline)(const PAPI *, const gchar *, gint, PAPIStatus *) ;
    gint (*mIsOffline)(const PAPI *, const gchar *, int *, PAPIStatus *) ;
    PAPIListenerId (*mAddListener)(const PAPI *, const gchar *, PAPIListener,
                                   void *, PAPIStatus *) ;
    void (*mRemoveListener)(const PAPI *, PAPIListenerId, PAPIStatus *) ;
    void (*mFreeLayerList)(PAPILayerList *) ;
    void (*mFreeStringList)(PAPIStringList *) ;
} ApocPapi ;

/** 
  Retrieves an initialised instance of the entry points list.
  The object returned must be destroyed with the destroyApocPapi 
  function when not in use anymore.
  */
ApocPapi *createApocPapi(GError **aError) ;
/**
  Disposes of an entry point list returned by createApocPapi.
  */
void destroyApocPapi(ApocPapi *aPAPI) ;

/**
  Holder for a connection to the PAPI.
  Contains the context of the connection and its state.
  */
typedef struct
{
    PAPI *mContext ;
    gboolean mDisconnected ;
} ApocPapiConnection ;

/**
  Constructor of a connection.
  */
ApocPapiConnection *createApocPapiConnection(ApocPapi *aPapi,
                                             PAPIConnectionListener aListener,
                                             void *aCallbackContext,
                                             GError **aError) ;
/**
  Destructor of a connection. 
  */
void destroyApocPapiConnection(ApocPapi *aPapi, 
                               ApocPapiConnection *aConnection) ;

#endif // Apoc_Papi_H

