/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either
 * the GNU General Public License Version 2 only ("GPL") or
 * the Common Development and Distribution License("CDDL")
 * (collectively, the "License"). You may not use this file
 * except in compliance with the License. You can obtain a copy
 * of the License at www.sun.com/CDDL or at COPYRIGHT. See the
 * License for the specific language governing permissions and
 * limitations under the License. When distributing the software,
 * include this License Header Notice in each file and include
 * the License file at /legal/license.txt. If applicable, add the
 * following below the License Header, with the fields enclosed
 * by brackets [] replaced by your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by
 * only the CDDL or only the GPL Version 2, indicate your
 * decision by adding "[Contributor] elects to include this
 * software in this distribution under the [CDDL or GPL
 * Version 2] license." If you don't indicate a single choice
 * of license, a recipient has the option to distribute your
 * version of this file under either the CDDL, the GPL Version
 * 2 or to extend the choice of license to its licensees as
 * provided above. However, if you add GPL Version 2 code and
 * therefore, elected the GPL Version 2 license, then the
 * option applies only if the new code is made subject to such
 * option by the copyright holder.
 */


#include "apoc-node.h"
#include "apoc-tree.h"
#include "apoc-tree-builder.h"
#include "common-utils.h"

#include <string.h>

static const gchar *MODULE =  "[ApocNode]" ;

/** Possible configuration node types */
typedef enum
{
    nodeType_Root = 0,  /** Special type of structural node */
    nodeType_Structural,/** Hierarchy member */
    nodeType_Property   /** Leaf node holding settings */
} ApocNodeType ;

/** Configuration node definition */
struct privApocNode
{
    ApocNodeType mType ;    /** Node type */
    union
    {
        ApocNode *asNode ;  /** Structural nodes point to their parent */
        ApocTree *asTree ;  /** Root nodes point to their tree */
    } mParent ;
    gchar *mName ;          /** Node name */
    gboolean mIsFinalised ; /** Is the node finalised? */
}  ;

/** Structural node definition */
struct privApocStructuralNode
{
    ApocNode mNode ;        /** Fake inheritance */
    GHashTable *mChildren ; /** List of children */
} ;

/**
  Constructor from parsing.
  */
ApocStructuralNode *createApocStructuralNode(const gchar *aName, 
                                             gboolean aIsFinalised,
                                             GError **aError)
{
    ApocNode *retCode = NULL ;

    g_return_val_if_fail(aName != NULL && *aName != 0, NULL) ;
    retCode = (ApocNode *) g_new0(ApocStructuralNode, 1) ;
    retCode->mType = nodeType_Structural ;
    retCode->mName = g_strdup(aName) ;
    retCode->mIsFinalised = aIsFinalised ;
    return (ApocStructuralNode *) retCode ;
}

/**
  Adds a child to the hashtable.
  */
void apocNodeAddChild(ApocStructuralNode *aNode, ApocNode *aChild, 
                      GError **aError)
{
    g_return_if_fail(aNode != NULL && aChild != NULL) ;
    if (aChild->mParent.asNode != NULL && 
            aChild->mParent.asNode == (ApocNode *) aNode) 
    { 
        return ; 
    }
    if (aNode->mChildren == NULL)
    {
        aNode->mChildren = g_hash_table_new(g_str_hash, g_str_equal) ;
    }
    g_hash_table_insert(aNode->mChildren, aChild->mName, aChild) ;
    aChild->mParent.asNode = (ApocNode *) aNode ;
}

/**
  Removes a child from the hashtable and destroys it and its
  children.
  */
void apocNodeDestroyChild(ApocStructuralNode *aNode, const ApocNode *aChild)
{
    g_return_if_fail(aNode != NULL && aChild != NULL) ;
    if (aNode->mChildren == NULL) { return ; }
    g_hash_table_remove(aNode->mChildren, aChild->mName) ;
    destroyApocNode((ApocNode *) aChild, TRUE) ;
}

/**
  Child node navigation by providing the name of the child.
  */
const ApocNode *apocNodeFindChild(const ApocStructuralNode *aNode, 
                                  const gchar *aChildName) 
{
    g_return_val_if_fail(aNode != NULL, NULL) ;
    if (aNode->mChildren == NULL) { return NULL ; }
    return g_hash_table_lookup(aNode->mChildren, aChildName) ;
}

/**
  Sets the node as a root one and fills its link to the tree.
  */
void apocNodeSetTree(ApocNode *aNode, ApocTree *aTree) 
{
    g_return_if_fail(aNode != NULL && aTree != NULL) ;
    aNode->mParent.asTree = aTree ;
    aNode->mType = nodeType_Root ;
}

/**
  Callback to be used in the destruction of children of 
  a structural node.
  */
void childDestructor(gpointer aName, gpointer aNode, gpointer aNull)
{
    if (aNode != NULL) 
    {
        destroyApocNode(aNode, TRUE) ;
    }
}

/**
  Destructor of a structural node. 
  Optionally goes through its children.
  */
static void destroyApocStructuralNode(ApocStructuralNode *aNode, 
                                      gboolean aDestroyChildren)
{
    if (aNode != NULL)
    {
        if (aNode->mChildren != NULL)
        {
            if (aDestroyChildren) 
            {
                g_hash_table_foreach(aNode->mChildren, childDestructor, NULL) ;
            }
            g_hash_table_destroy(aNode->mChildren) ;
        }
        g_free(aNode) ;
    }
}

/** Value holder definition */
typedef struct 
{
    ApocValueType mType ;               /** Value type */
    gboolean mIsLocalised ;             /** Is is localised? */
    union
    {
        gchar *asString ;               /** Simple value as string */
        GSList *asList ;                /** List of values as string */
        GHashTable *asLocalisedMap ;    /** Map of locales to values */
    } mValue ;
} ApocValue ;

/**
  Constructor.
  */
static ApocValue *createApocValue(ApocValueType aType, GError **aError)
{
    ApocValue *retCode = NULL ;

    retCode = g_new0(ApocValue, 1) ;
    if (aType == valueType_Unknown) { aType = valueType_String ; }
    retCode->mType = aType ;
    return retCode ;
}

/** 
  Fake locale that gets used to store default values for 
  localised properties. 
  */
static const gchar *kDefaultLocales [] = { "default", NULL } ;

/**
  Finds the best localised value match.
  `*/
static gpointer apocValueBestLocaleMatch(const ApocValue *aValue,
                                         const gchar **aLocales)
{
    gpointer retCode = NULL ;
    const gchar **currentLocale = NULL ;

    if (aValue->mValue.asLocalisedMap == NULL) { return retCode ; }
    currentLocale = aLocales != NULL ? aLocales : kDefaultLocales ;
    while (*currentLocale != NULL)
    {
        retCode = g_hash_table_lookup(aValue->mValue.asLocalisedMap, 
                                      *currentLocale) ;
        if (retCode != NULL) { break ; }
        ++ currentLocale ;
    }
    return retCode ;
}

/**
  Checks if a string matches the replacement pattern.
  */
static gboolean isPlaceholder(const gchar *aString, 
                              guint aStringLength,
                              const gchar **aMatchStart,
                              gchar **aMatchEnd)
{
    static const gchar *kPlaceholderPrefix = "[apoc." ;
    static const guint kPlaceholderPrefixLength = 6 ;

    if (*aString == '[' && aStringLength > kPlaceholderPrefixLength + 1 &&
            strncmp(aString, kPlaceholderPrefix, kPlaceholderPrefixLength) == 0)
    {
        *aMatchStart = aString + kPlaceholderPrefixLength ;
        *aMatchEnd = strchr(*aMatchStart, ']') ;
        if (*aMatchEnd != NULL) { return TRUE ; }
    }
    return FALSE ;
}

/** Property node definition */
struct privApocPropertyNode
{
    ApocNode mNode ;    /** Fake inheritance */
    ApocValue *mValue ; /** Property value */
} ;

/**
  Finds a replacement value for a given name in the replacement tree.
  */
static const gchar *findReplacement(const ApocStructuralNode *aReplacements,
                                    const gchar *aName)
{
    const ApocNode *child = apocNodeFindChild(aReplacements, aName) ;

    if (child != NULL && apocNodeIsProperty(child))
    {
        ApocValue *value = ((const ApocPropertyNode *) child)->mValue ;

        if (value != NULL && !value->mIsLocalised && 
                (value->mType & valueType_ListMask) == 0)
        {
            return value->mValue.asString ;
        }
    }
    return NULL ;
}

/**
  Creates a GConf value from a string.
  Expands if necessary the contents of the value if they use
  placeholders, and uses the standard GConf utility method to 
  parse the string into a value of the proper type.
  */
static GConfValue *getExpandedGConfValue(
                                        GConfValueType aType,
                                        const gchar *aValue,
                                        const ApocStructuralNode *aReplacements,
                                        GError **aError)
{
    GConfValue *retCode = NULL ;
    GString *value = NULL ;
    guint length = strlen(aValue) ;

    g_return_val_if_fail(aValue != NULL, retCode) ;
    if (aReplacements != NULL) 
    {
        guint i = 0 ;

        for (i = 0 ; i < length ; ++ i) 
        {
            const gchar *matchStart = NULL ;
            gchar *matchEnd = NULL ;

            if (isPlaceholder(aValue + i, length - i, &matchStart, &matchEnd))
            {
                const gchar *replacement = NULL ;
                
                if (value == NULL) { value = g_string_new_len(aValue, i) ; }
                *matchEnd = 0 ;
                replacement = findReplacement(aReplacements, matchStart) ;
                *matchEnd = ']' ;
                if (replacement != NULL)
                {
                    g_string_append(value, replacement) ;
                }
                i = matchEnd - aValue ;
            }
            else if (value != NULL) { g_string_append_c(value, aValue [i]) ; }
        }
    }
    if (value != NULL) 
    {   /* The original string was replaced. */
        retCode = gconf_value_new_from_string(aType, value->str, aError) ;
        g_string_free(value, TRUE) ;
    }
    else { retCode = gconf_value_new_from_string(aType, aValue, aError) ; }
    return retCode ;
}

/**
  Converts an Apoc value into a GConf one.
  Instantiates properly typed GConfValue object depending on
  the Apoc type (non-compatible types are just ignored for the
  moment), the values are converted from string by the GConf 
  utility method.
  A special replacement tree can be used to expand placeholders 
  in the property value.
  */
static GConfValue *apocValueToGConfValue(
                                        const ApocValue *aValue, 
                                        const gchar **aLocales, 
                                        const ApocStructuralNode *aReplacements,
                                        GError **aError)
{
    GConfValueType type = GCONF_VALUE_INVALID ;
    ApocValueType valueType = valueType_Unknown ;
    GConfValue *retCode = NULL ;

    g_return_val_if_fail(aValue != NULL, retCode) ;
    if (aValue->mValue.asString == NULL)
    {   /* Right, we got a property, but its value has never been
           set, which is not nice, so we'll just ignore the whole thing. */
        gconf_log(GCL_WARNING,
                  _("%s Trying to access an empty property node."), MODULE) ;
        return NULL ;
    }
    valueType = aValue->mType ;
    if ((valueType & valueType_ListMask) != 0)
    {
        retCode = gconf_value_new(GCONF_VALUE_LIST) ;
        valueType &= ~valueType_ListMask ;
    }
    switch (valueType)
    {
        case valueType_Boolean: type = GCONF_VALUE_BOOL ; break ;
        case valueType_Short: 
        case valueType_Int: type = GCONF_VALUE_INT ; break ;
        case valueType_Double: type = GCONF_VALUE_FLOAT ; break ;
        case valueType_String: type = GCONF_VALUE_STRING ; break ;
        case valueType_Long:
        case valueType_HexBinary:
        case valueType_Unknown:
        default:
            break ;
    }
    if (type == GCONF_VALUE_INVALID) 
    {
        gconf_log(GCL_WARNING, 
                  _("%s Invalid value type (%d)."), MODULE, valueType) ;
        return retCode ;
    }
    if (retCode == NULL)
    {   /* Simple type instantiation */
        gchar *value = NULL ;

        if (aValue->mIsLocalised)
        {
            value = apocValueBestLocaleMatch(aValue, aLocales) ;
        }
        else { value = aValue->mValue.asString ; }
        if (value == NULL) { value = "" ; }
        retCode = getExpandedGConfValue(type, value, aReplacements, aError) ;
    }
    else
    {   /* List type, the object already exists but needs to be filled */
        GSList *apocValueList = NULL ;
        GSList *gconfValueList = NULL ;

        if (aValue->mIsLocalised)
        {
            apocValueList = apocValueBestLocaleMatch(aValue, aLocales) ;
        }
        else { apocValueList = aValue->mValue.asList ; }
        gconf_value_set_list_type(retCode, type) ;
        while (apocValueList != NULL)
        {
            GConfValue *newValue = NULL ;
            GError *error = NULL ;

            newValue = getExpandedGConfValue(type, apocValueList->data,
                                             aReplacements, &error) ;
            if (error == NULL) 
            {
                gconfValueList = g_slist_append(gconfValueList, newValue) ;
            }
            else 
            {
                gconf_log(GCL_ERR, 
                          _("%s Error creating value from string: %s"),
                          MODULE, error->message) ;
                g_error_free(error) ; 
            }
            apocValueList = g_slist_next(apocValueList) ;
        }
        gconf_value_set_list(retCode, gconfValueList) ;
    }
    return retCode ;
}

/** Deatructor of a value list in an ApocValue. */
static void destroyValueList(GSList *aList)
{
    GSList *iterator = aList ;

    if (aList == NULL) { return ; }
    do
    {
        g_free(iterator->data) ; 
        iterator = g_slist_next(iterator) ;
    } while (iterator != NULL) ; 
    g_slist_free(aList) ;
}

/** Destructor of the elements of the localised values hashtable. */
static void destroyLocalisedValue(gpointer aLocale, 
                                  gpointer aValue,
                                  gpointer aApocValue)
{
    ApocValue *apocValue = aApocValue ;
    
    g_free(aLocale) ;
    if ((apocValue->mType & valueType_ListMask) == 0)
    {
        g_free(aValue) ;
    }
    else
    {
        destroyValueList((GSList *) aValue) ;
    }
}

/** Destructor of an ApocValue */
static void destroyApocValue(ApocValue *aValue)
{
    if (aValue == NULL) { return ; }
    if (aValue->mValue.asString != NULL) 
    {
        if (aValue->mIsLocalised)
        {
            if (aValue->mValue.asLocalisedMap != NULL)
            {
                g_hash_table_foreach(aValue->mValue.asLocalisedMap, 
                                    destroyLocalisedValue,
                                    aValue) ;
                g_hash_table_destroy(aValue->mValue.asLocalisedMap) ;
            }
        }
        else
        {
            if ((aValue->mType & valueType_ListMask) == 0)
            {
                g_free(aValue->mValue.asString) ;
            }
            else
            {
                destroyValueList(aValue->mValue.asList) ;
            }
        }
    }
    g_free(aValue) ;
}

/**
  Constructor from parsing.
  */
ApocPropertyNode *createApocPropertyNode(const gchar *aName, 
                                         gboolean aIsFinalised,
                                         ApocValueType aType,
                                         GError **aError)
{
    ApocPropertyNode *retCode = NULL ;

    if (aName == NULL || *aName == 0) { return retCode ; }
    retCode = g_new0(ApocPropertyNode, 1) ;
    ((ApocNode *) retCode)->mType = nodeType_Property ;
    ((ApocNode *) retCode)->mName = g_strdup(aName) ;
    ((ApocNode *) retCode)->mIsFinalised = aIsFinalised ;
    retCode->mValue = createApocValue(aType, aError) ;
    return retCode ;
}

/**
  Adds a string to the value contents.
  */
void apocNodeAddValue(ApocPropertyNode *aNode, const gchar *aLocale,
                      const gchar *aSeparator, gchar *aValue,
                      GError **aError)
{
    ApocValue *value = NULL ;
    gchar *singleValue = NULL ;
    GSList *valueList = NULL ;

    g_return_if_fail(aNode != NULL) ;
    value = aNode->mValue ;
    if ((value->mType & valueType_ListMask) == 0)
    {
        if (aValue == NULL) { aValue = "" ; }
        singleValue = g_strdup(aValue) ;
    }
    else
    {
        const gchar *oldToken = aValue ;
        const gchar *nextToken = NULL ;
        gint separatorLength = 0 ;
        
        if (oldToken == NULL) { oldToken = "" ; }
        if (aSeparator == NULL || *aSeparator == 0) { aSeparator = " " ; }
        separatorLength = strlen(aSeparator) ;
        do
        {
            nextToken = strstr(oldToken, aSeparator) ;
            if (nextToken != NULL)
            {
                valueList = g_slist_append(valueList, 
                                           g_strndup(oldToken, 
                                                     nextToken - oldToken)) ;
                oldToken = nextToken + separatorLength ;
            }
            else
            {
                valueList = g_slist_append(valueList, g_strdup(oldToken)) ;
            }
        } while (nextToken != NULL) ;
    }
    if (value->mIsLocalised)
    {
        if (aLocale == NULL || *aLocale == 0) 
        {
            aLocale = kDefaultLocales [0] ; 
        }
        if (value->mValue.asLocalisedMap == NULL) 
        {
            value->mValue.asLocalisedMap = g_hash_table_new(g_str_hash, 
                                                            g_str_equal) ;
        }
        g_hash_table_insert(value->mValue.asLocalisedMap,
                            g_strdup(aLocale),
                            singleValue != NULL ? (gpointer) singleValue : 
                                                  (gpointer) valueList) ;
    }
    else
    {
        if (singleValue != NULL) { value->mValue.asString = singleValue ; }
        else { value->mValue.asList = valueList ; }
    }
}

/**
  Destructor of a property node.
  */
static void destroyApocPropertyNode(ApocPropertyNode *aNode)
{
    if (aNode != NULL)
    {
        destroyApocValue(aNode->mValue) ;
        g_free(aNode) ;
    }
}

/**
  Maps a property node to a GConfValue object. 
  Checks that this is a property node, maps type and value.
  Uses if present a replacement tree to expand placeholder strings
  in the value.
  */
GConfValue *apocNodeToGConfValue(const ApocNode *aNode,
                                 const gchar **aLocales,
                                 const ApocNode *aReplacements,
                                 GError **aError)
{
    g_return_val_if_fail(aNode != NULL, NULL) ;
    if (aNode->mType != nodeType_Property)
    {
        gconf_log(GCL_WARNING,
                  _("%s Trying to turn a structural node %s into a value."),
                  MODULE, aNode->mName) ;
        return NULL ;
    }
    if (aReplacements != NULL && apocNodeIsProperty(aReplacements)) 
    {
        aReplacements = NULL ;
    }
    return apocValueToGConfValue(((const ApocPropertyNode *) aNode)->mValue,
                                 aLocales, 
                                 (const ApocStructuralNode *) aReplacements, 
                                 aError) ;
}

/**
  Returns the last modification time of a node.
  Goes to its parent until it gets to the top and can ask
  the tree, which actually contains that information.
  */
static GTime apocNodeGetModificationTime(const ApocNode *aNode)
{
    if (aNode->mType == nodeType_Root)
    {
        return apocTreeGetModificationTime(aNode->mParent.asTree) ;
    }
    return apocNodeGetModificationTime(aNode->mParent.asNode) ;
}

/**
  Returns the meta-information associated with the node.
  Actually talks to the tree as the timestamp is treewide
  and the rest is not valued.
  */
GConfMetaInfo *apocNodeToGConfMetaInfo(const ApocNode *aNode,
                                       GError **aError)
{
    static const gchar *kApocModifier = "ApocAdministrator" ;
    GConfMetaInfo *retCode = NULL ;
    
    g_return_val_if_fail(aNode != NULL, retCode) ;
    retCode = gconf_meta_info_new() ;
    gconf_meta_info_set_mod_time(retCode, apocNodeGetModificationTime(aNode)) ;
    gconf_meta_info_set_mod_user(retCode, kApocModifier) ;
    return retCode ;
}

/**
  Callback method for iteration through nodes looking for properties.
  The user data is a GSList which must be filled with ApocNode instances.
  */ 
static void propertyExtractor(gpointer aKey, 
                              gpointer aValue, 
                              gpointer aUserData)
{
    ApocNode *child = aValue ;

    if (child->mType == nodeType_Property)
    {   /* Bingo */
        GSList **data = aUserData ;

        *data = g_slist_append(*data, child) ;
    }
}

/**
  Lists the properties under a node.
  Checks the node is a structural node and finds its property children.
  */
GSList *apocNodeListProperties(const ApocNode *aNode, 
                               GError **aError)
{
    GSList *retCode = NULL ;
    const ApocStructuralNode *node = NULL ;

    g_return_val_if_fail(aNode != NULL, retCode) ;
    if (aNode->mType == nodeType_Property) 
    {
        gconf_log(GCL_WARNING, 
                  _("%s Trying to list properties of property node %s."),
                  MODULE, aNode->mName) ;
        return retCode ;
    }
    node = (const ApocStructuralNode *) aNode ;
    if (node->mChildren == NULL) { return retCode ; }
    g_hash_table_foreach(node->mChildren, propertyExtractor, &retCode) ;
    return retCode ;
}

/**
  Returns the finalised status of a node.
  */
gboolean apocNodeIsFinalised(const ApocNode *aNode)
{
    g_return_val_if_fail(aNode != NULL, FALSE) ;
    return aNode->mIsFinalised ;
}

/**
  Visitor method for nodes that sets their finalised status.
  */
void nodeFinaliser(gpointer aName, gpointer aChild, gpointer aIsFinalised)
{
    ApocNode *node = aChild ;
    
    g_return_if_fail(aChild != NULL && aIsFinalised != NULL) ;
    apocNodeSetFinalised(node, *((gboolean *) aIsFinalised)) ;
}

/**
  Sets the finalised state of a node.
  */
void apocNodeSetFinalised(ApocNode *aNode, gboolean aIsFinalised)
{
    g_return_if_fail(aNode != NULL) ;
    aNode->mIsFinalised = aIsFinalised ;
    if (aNode->mType != nodeType_Property)
    {
        ApocStructuralNode *node = (ApocStructuralNode *) aNode ;

        if (node->mChildren != NULL)
        {
            g_hash_table_foreach(node->mChildren, 
                                 nodeFinaliser, &aIsFinalised) ;
        }
    }
}

/**
  Indicates whether the node is a property one.
  */
gboolean apocNodeIsProperty(const ApocNode *aNode)
{
    g_return_val_if_fail(aNode != NULL, FALSE) ;
    return aNode->mType == nodeType_Property ;
}

/**
  Returns the name of a node.
  */
const gchar *apocNodeGetName(const ApocNode *aNode)
{
    g_return_val_if_fail(aNode != NULL, NULL) ;
    return aNode->mName ;
}

/**
  Builds recursively as a dynamic string the full name of a node.
  */
static GString *apocNodeBuildFullName(const ApocNode *aNode)
{
    GString *retCode = NULL ;

    g_return_val_if_fail(aNode != NULL, retCode) ;
    if (aNode->mType != nodeType_Root)
    {
        retCode = apocNodeBuildFullName(aNode->mParent.asNode) ;
        retCode = g_string_append(retCode, "/") ;
        retCode = g_string_append(retCode, aNode->mName) ;
    }
    else
    {
        retCode = apocTreeGetPath(aNode->mParent.asTree) ;
    }
    return retCode ;
}

/**
  Returns the full name of the node. 
  */
gchar *apocNodeGetFullName(const ApocNode *aNode)
{
    GString *name = apocNodeBuildFullName(aNode) ;

    return name == NULL ? NULL : g_string_free(name, FALSE) ;
}

/**
  Callback method for iteration through nodes looking for subnodes.
  The user data is a GSList of allocated strings.
  */
static void subnodeExtractor(gpointer aKey, gpointer aValue, gpointer aUserData)
{
    const ApocNode *node = (const ApocNode *) aValue ;
    
    if (node->mType != nodeType_Property)
    {
        GSList **list = (GSList **) aUserData ;

        *list = g_slist_append(*list, g_strdup(aKey)) ;
    }
}

/**
  Lists the subnodes under a node.
  Checks the node is a structural one and finds its structural 
  children.
  */
GSList *apocNodeListSubnodes(const ApocNode *aNode,
                             GSList *aList)
{
    g_return_val_if_fail(aNode != NULL, aList) ;
    if (aNode->mType == nodeType_Property)
    {
        gconf_log(GCL_WARNING, 
                  _("%s Trying to list subnodes of property node %s."),
                  MODULE, aNode == NULL ? "NULL" : aNode->mName) ;
        return aList ;
    }
    if (((const ApocStructuralNode *) aNode)->mChildren == NULL)
    {
        return aList ;
    }
    g_hash_table_foreach(((const ApocStructuralNode *) aNode)->mChildren,
                         subnodeExtractor, &aList) ;
    return aList ;
}

/**
  Definition of the context for the visit of a node's children
  and their comparison to nothing, i.e their processing as standalone.
  */
typedef struct
{
    /**
      Indicates whether the nodes visited are the "first" or the
      "second" in the original comparison. */
    gboolean mAreFirst ;
    ApocNodeCompareAction mCallback ;   /** Callback on difference */
    gpointer mUserData ;                /** Context for callback */
} ApocNodeNullComparisonContext ;

/** Forward declaration of the comparison between a node and null. */
static void apocNodeCompareToNull(const ApocNode *aNode,
                                  ApocNodeCompareAction aCallback,
                                  gpointer aUserData,
                                  gboolean aIsFirst) ;

/**
  Visit function for comparison of child nodes to nothing.
  */
static void compareChildrenToNull(gpointer aName, gpointer aChild,
                                  gpointer aContext)
{
    ApocNodeNullComparisonContext *context = aContext ;
    
    g_return_if_fail(aChild != NULL && aContext != NULL) ;
    apocNodeCompareToNull(aChild, context->mCallback, context->mUserData,
                          context->mAreFirst) ;
}

/**
  Compares a node to nothing.
  For property nodes, invokes the action callback, placing the 
  argument node in the first or second position to keep the original
  comparison order.
  For structural nodes, visits the children and recurses.
  */
static void apocNodeCompareToNull(const ApocNode *aNode, 
                                  ApocNodeCompareAction aCallback,
                                  gpointer aUserData,
                                  gboolean aIsFirst)
{
    g_return_if_fail(aNode != NULL && aCallback != NULL) ;
    if (aNode->mType == nodeType_Property)
    {
        aCallback(aIsFirst ? aNode : NULL, aIsFirst ? NULL : aNode, aUserData) ;
    }
    else
    {
        const ApocStructuralNode *node = (const ApocStructuralNode *) aNode ;

        if (node->mChildren != NULL)
        {
            ApocNodeNullComparisonContext context ;

            context.mAreFirst = aIsFirst ;
            context.mCallback = aCallback ;
            context.mUserData = aUserData ;
            g_hash_table_foreach(node->mChildren, compareChildrenToNull,
                                 &context) ;
        }
    }
}

/**
  Definition of the context for the visit of two nodes' children
  and the comparison thereof.
  The comparison is done in two passes. In the first one, the old 
  nodes are compared to potentially new nodes (identified by their
  name). In the second one, the remaining new nodes (i.e those with
  no old counterpart) are dealt with, i.e compared to nothing.
  */
typedef struct
{
    GHashTable *mOtherChildren ;        /** Children from the other node */
    ApocNodeCompareAction mCallback ;   /** Callback on difference */
    gpointer mUserData ;                /** Context for callback */
} ApocNodeComparisonContext ;

/**
  First pass visit function (see above).
  The nodes being visited are considered old nodes, and the 
  contents of the children list in the context potentially new
  ones.
  */
static void compareChildren(gpointer aName, gpointer aChild, gpointer aContext)
{
    ApocNodeComparisonContext *context = aContext ;
    const ApocNode *newNode = NULL ;

    g_return_if_fail(aName != NULL && aChild != NULL && aContext != NULL) ;
    if (context->mOtherChildren != NULL)
    {
        newNode = g_hash_table_lookup(context->mOtherChildren, aName) ;
    }
    apocNodeCompare(aChild, newNode, context->mCallback, context->mUserData) ;
}

/**
  Second pass visit function (see above).
  The nodes being visited are considered new nodes, and only the 
  ones without counterpart in the old list are processed.
  */
static void compareUniqueChildren(gpointer aName, gpointer aChild,
                                  gpointer aContext)
{
    ApocNodeComparisonContext *context = aContext ;

    g_return_if_fail(aName != NULL && aChild != NULL && aContext != NULL) ;
    if (context->mOtherChildren == NULL ||
            g_hash_table_lookup(context->mOtherChildren, aName) == NULL)
    {
        apocNodeCompare(NULL, aChild, context->mCallback, context->mUserData) ;
    }
}

/**
  Utility to test equality of two string lists.
  Since the lists are not ordered and can contain duplicates, 
  we have to first check that they have the same size and then
  for each item of the first one find an equal one in the second,
  making sure to use up items of the second list only once.
  */
static gboolean apocListValueIsEqual(GSList *aList1, GSList *aList2)
{
    GSList *iterator1 = aList1 ;
    guint length = 0 ;
    guchar *used = NULL ;
    gboolean retCode = TRUE ;

    if (aList1 == NULL) { return aList2 == NULL ; }
    else if (aList2 == NULL) { return FALSE ; }
    length = g_slist_length(aList1) ;
    if (g_slist_length(aList2) != length) { return FALSE ; }
    used = g_new0(guchar, length) ;
    do
    {
        GSList *iterator2 = aList2 ;
        guint i = 0 ;

        do
        {
            if (used [i] == 0 &&
                    strcmp(iterator1->data, iterator2->data) == 0) 
            {
                used [i] = 1 ;
                break ;
            }
            ++ i ;
            iterator2 = g_slist_next(iterator2) ;
        } while (iterator2 != NULL) ;
        if (iterator2 == NULL) 
        {
            retCode = FALSE ; 
            break ;
        }
        iterator1 = g_slist_next(iterator1) ;
    } while (iterator1 != NULL) ;
    g_free(used) ;
    return retCode ;
}

/**
  Checks for the equality of two simple values.
  */
static gboolean apocSimpleValueIsEqual(const gchar *aValue1, 
                                       const gchar *aValue2)
{
    if (aValue1 == NULL || aValue2 == NULL) { return aValue1 == aValue2 ; }
    return strcmp(aValue1, aValue2) == 0 ;
}

/**
  Structure used to store the context of the comparison of 
  two localised values.
  */
typedef struct
{
    ApocValueType mType ;
    GHashTable *mOther ;
    gboolean mAreEqualSoFar ;
} ApocLocalisedValueComparisonContext ;

/**
  Traversal method in a localised value hashtable which compares it
  with another value.
  */
static void compareLocalisedValue(gpointer aKey, 
                                  gpointer aValue,
                                  gpointer aContext)
{
    ApocLocalisedValueComparisonContext *context = aContext ;

    if (!context->mAreEqualSoFar) { return ; }
    if ((context->mType & valueType_ListMask) == 0)
    {
        context->mAreEqualSoFar = 
            apocSimpleValueIsEqual(aValue,
                                   g_hash_table_lookup(context->mOther, aKey)) ;
    }
    else
    {
        context->mAreEqualSoFar = 
            apocListValueIsEqual(aValue,
                                 g_hash_table_lookup(context->mOther, aKey)) ;
    }
}

/**
  Checks for the equality of two localised values.
  */
static gboolean apocLocalisedValueIsEqual(GHashTable *aValue1, 
                                          GHashTable *aValue2,
                                          ApocValueType aType)
{
    ApocLocalisedValueComparisonContext context ;
    
    if (aValue1 == NULL || aValue2 == NULL) { return aValue1 == aValue2 ; }
    context.mType = aType ;
    context.mOther = aValue2 ;
    context.mAreEqualSoFar = FALSE ;
    g_hash_table_foreach(aValue1, compareLocalisedValue, &context) ;
    return context.mAreEqualSoFar ;
}

/**
  Tests for the equality of two property values.
  */
static gboolean apocValueIsEqual(const ApocValue *aValue1,
                                 const ApocValue *aValue2)
{
    g_return_val_if_fail(aValue1 != NULL && aValue2 != NULL, FALSE) ;
    if (aValue1->mType != aValue2->mType ||
            aValue1->mIsLocalised != aValue2->mIsLocalised) { return FALSE ; }
    if (aValue1->mIsLocalised) 
    {
        return apocLocalisedValueIsEqual(aValue1->mValue.asLocalisedMap, 
                                         aValue2->mValue.asLocalisedMap,
                                         aValue1->mType) ;
    }
    if ((aValue1->mType & valueType_ListMask) == 0)
    {
        return apocSimpleValueIsEqual(aValue1->mValue.asString,
                                      aValue2->mValue.asString) ;
    }
    return apocListValueIsEqual(aValue1->mValue.asList, 
                                aValue2->mValue.asList) ;
}

/**
  Compares two non-null property nodes.
  */
static void apocPropertyNodeCompare(const ApocNode *aNode1,
                                    const ApocNode *aNode2,
                                    ApocNodeCompareAction aCallback,
                                    gpointer aUserData)
{
    g_return_if_fail(aNode1 != NULL && aNode2 != NULL && aCallback != NULL) ;
    if (aNode1->mIsFinalised != aNode2->mIsFinalised ||
            !apocValueIsEqual(((const ApocPropertyNode *) aNode1)->mValue, 
                              ((const ApocPropertyNode *) aNode2)->mValue))
    {
        aCallback(aNode1, aNode2, aUserData) ;
    }
}

/**
  Compares a pair of nodes and calls a function for each differing pair.
  The only nodes considered for callback are the property ones.
  Nodes whose type (structural vs property) do not match are ignored.
  */
void apocNodeCompare(const ApocNode *aNode1, const ApocNode *aNode2,
                     ApocNodeCompareAction aCallback, gpointer aUserData)
{
    g_return_if_fail((aNode1 != NULL || aNode2 != NULL) && aCallback != NULL) ;
    if (aNode1 == NULL) 
    {
        apocNodeCompareToNull(aNode2, aCallback, aUserData, FALSE) ;
    }
    else if (aNode2 == NULL)
    {
        apocNodeCompareToNull(aNode1, aCallback, aUserData, TRUE) ;
    }
    else 
    {
        if (aNode1->mType != aNode2->mType)
        {   
            gconf_log(GCL_WARNING, 
                      _("%s Node %s changed its type over time!"),
                      MODULE, aNode1->mName) ;
            return ;
        }
        if (aNode1->mType == nodeType_Property)
        {
            apocPropertyNodeCompare(aNode1, aNode2, 
                                    aCallback, aUserData) ;
        }
        else
        {
            const ApocStructuralNode *node1 = 
                                        (const ApocStructuralNode *) aNode1 ;
            const ApocStructuralNode *node2 = 
                                        (const ApocStructuralNode *) aNode2 ;
            ApocNodeComparisonContext context ;

            context.mCallback = aCallback ;
            context.mUserData = aUserData ;
            if (node1->mChildren != NULL)
            {   /* First a pass on all children of aNode1 and their
                   potential counterparts in aNode2. */
                context.mOtherChildren = node2->mChildren ;
                g_hash_table_foreach(node1->mChildren, compareChildren, 
                                     &context) ;
            }
            if (node2->mChildren != NULL)
            {   /* Then all the children of aNode2 with no counterparts
                   in aNode1 (already done in the first pass). */
                context.mOtherChildren = node1->mChildren ;
                g_hash_table_foreach(node2->mChildren, compareUniqueChildren,
                                     &context) ;
            }
        }
    }
}

/**
  Destructor of a node.
  Cleans up its data and if necessary destroys its children.
  */
void destroyApocNode(ApocNode *aNode, gboolean aDestroyChildren)
{
    if (aNode != NULL)
    {
        /** Cleanup of the common part */
        g_free(aNode->mName) ;
        /** Specific node type dependent destruction */
        if (aNode->mType == nodeType_Property)
        {
            destroyApocPropertyNode((ApocPropertyNode *) aNode) ;
        }
        else 
        { 
            destroyApocStructuralNode((ApocStructuralNode *) aNode, 
                                      aDestroyChildren) ; 
        }
    }
}


