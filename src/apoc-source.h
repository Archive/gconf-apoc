/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either
 * the GNU General Public License Version 2 only ("GPL") or
 * the Common Development and Distribution License("CDDL")
 * (collectively, the "License"). You may not use this file
 * except in compliance with the License. You can obtain a copy
 * of the License at www.sun.com/CDDL or at COPYRIGHT. See the
 * License for the specific language governing permissions and
 * limitations under the License. When distributing the software,
 * include this License Header Notice in each file and include
 * the License file at /legal/license.txt. If applicable, add the
 * following below the License Header, with the fields enclosed
 * by brackets [] replaced by your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by
 * only the CDDL or only the GPL Version 2, indicate your
 * decision by adding "[Contributor] elects to include this
 * software in this distribution under the [CDDL or GPL
 * Version 2] license." If you don't indicate a single choice
 * of license, a recipient has the option to distribute your
 * version of this file under either the CDDL, the GPL Version
 * 2 or to extend the choice of license to its licensees as
 * provided above. However, if you add GPL Version 2 code and
 * therefore, elected the GPL Version 2 license, then the
 * option applies only if the new code is made subject to such
 * option by the copyright holder.
 */


#ifndef ApocSource_H
#define ApocSource_H

#include <gconf/gconf.h>

#include "gconf-backend.h"

typedef struct privApocSource ApocSource ;

/**
  Returns a new Apoc source object. 
  */
ApocSource *createApocSource(const gchar *aAddress, GError **aError) ;

/**
  Retrieves the GConfValue corresponding to a given key in a source.
  */
GConfValue *apocSourceQueryValue(const ApocSource *aSource, const gchar *aKey,
                                 const gchar **aLocales, GError **aError) ;

/**
  Retrieves the GConfMetaInfo corresponding to a given key in a source.
  */
GConfMetaInfo *apocSourceQueryMetaInfo(const ApocSource *aSource, 
                                       const gchar *aKey,
                                       GError **aError) ;

/**
  Retrieves the list of all keys in a directory for a source.
  */
GSList *apocSourceAllEntries(const ApocSource *aSource, const gchar *aDirectory,
                             const gchar **aLocales, GError **aError) ;

/**
  Retrieves the list of all subdirectories in a directory for a source.
  */
GSList *apocSourceAllSubdirs(const ApocSource *aSource, const gchar *aDirectory,
                             GError **aError) ;

/**
  Tests whether a subdirectory exists in a directory for a source.
  */
gboolean apocSourceDirExists(const ApocSource *aSource, const gchar *aDirectory,
                             GError **aError) ;

/**
  Destroys an Apoc source object.
  */
void destroyApocSource(ApocSource *aSource) ;

/**
  Cleans up the cache of a source.
  */
void apocSourceClearCache(const ApocSource *aSource) ;

/**
  Initialises the source listeners from the client listeners list.
  */
void apocSourceInitListeners(ApocSource *aSource,
                             GConfSourceNotifyFunc aCallback,
                             gpointer aUserData) ;

/**
  Updates the source listeners with a new client one.
  */
void apocSourceNewListener(ApocSource *aSource, const gchar *aLocation) ;

#endif /* ApocSource_H */

