/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either
 * the GNU General Public License Version 2 only ("GPL") or
 * the Common Development and Distribution License("CDDL")
 * (collectively, the "License"). You may not use this file
 * except in compliance with the License. You can obtain a copy
 * of the License at www.sun.com/CDDL or at COPYRIGHT. See the
 * License for the specific language governing permissions and
 * limitations under the License. When distributing the software,
 * include this License Header Notice in each file and include
 * the License file at /legal/license.txt. If applicable, add the
 * following below the License Header, with the fields enclosed
 * by brackets [] replaced by your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by
 * only the CDDL or only the GPL Version 2, indicate your
 * decision by adding "[Contributor] elects to include this
 * software in this distribution under the [CDDL or GPL
 * Version 2] license." If you don't indicate a single choice
 * of license, a recipient has the option to distribute your
 * version of this file under either the CDDL, the GPL Version
 * 2 or to extend the choice of license to its licensees as
 * provided above. However, if you add GPL Version 2 code and
 * therefore, elected the GPL Version 2 license, then the
 * option applies only if the new code is made subject to such
 * option by the copyright holder.
 */


#include <string.h>
#include "apoc-source.h"
#include "apoc-tree.h"
#include "apoc-tree-access.h"
#include "gconf-backend.h"
#include "common-utils.h"

static const gchar *MODULE =  "[ApocSource]" ;

/** 
  Structure holding the data about a tree change watch. 
  */
typedef struct
{
    ApocNodePath *mNodePath ;   /** Path to the tree being watched. */
    guint mTreeListenerId ;     /** Corresponding tree listener id. */
    const ApocSource *mSource ; /** Corresponding source. */
} ApocSourceTreeWatcher ;

/**
  Structure holding the information associated to an Apoc data source.
  */
struct privApocSource 
{
    /** Fake inheritance */
    GConfSource mSource ;
    /** Does the source access mandatory settings? */
    gboolean mMandatory ;
    /** Access to the configuration data trees. */
    ApocTreeAccess *mTreeAccess ;
    /** List of trees being watched for changes. */
    GSList *mTreeWatchers ;
    /** Callback in case of a change in the source. */
    GConfSourceNotifyFunc mCallback ;
    /** User data to be passed to the notification callback. */
    gpointer mUserData ;
    /** Flag indicating whether the source is disconnected (unusable). */
    gboolean mDisconnected ;
    /** Node path to the replacement nodes. */
    ApocNodePath *mReplacementPath ;
} ;

/**
  Constructor of an ApocSource.
  */
static ApocSource *newApocSource(gboolean aMandatory, 
                                 const gchar *aParameters,
                                 GError **aError)
{
    ApocSource *retCode = NULL ;

    retCode = g_new0(ApocSource, 1) ;
    retCode->mMandatory = aMandatory ;
    retCode->mTreeAccess = createApocTreeAccess(aParameters, aError) ;
    if (retCode->mTreeAccess == NULL) 
    {   /* The source is unavailable, mark it as disconnected. */
        retCode->mDisconnected = TRUE ;
    }
    retCode->mReplacementPath = apocNodePathGetReplacementPath() ;
    ((GConfSource *) retCode)->flags = GCONF_SOURCE_ALL_READABLE |
                                       GCONF_SOURCE_NEVER_WRITEABLE ;
    return retCode ;
}

/**
  Builds an ApocSource from a data source address.
  */
ApocSource *createApocSource(const gchar *aAddress, GError **aError)
{
    gchar *resourceString = NULL ;
    ApocSource *retCode = NULL ;
    gchar **flags = NULL ;
    gboolean mandatory = FALSE ;
    const gchar *parameters = NULL ;

    flags = gconf_address_flags(aAddress) ;
    if (flags == NULL || strcmp(*flags, "readonly") != 0 || flags [1] != NULL)
    {
        gconf_set_error(aError, GCONF_ERROR_BAD_ADDRESS,
                        _("%s Only 'readonly' address flag supported."), 
                        MODULE) ;
        g_strfreev(flags) ;
        return retCode ;
    }
    g_strfreev(flags) ;
    resourceString = gconf_address_resource(aAddress) ;
    if (resourceString != NULL)
    {
        gchar *atSign = NULL ;

        atSign = strchr(resourceString, '@') ;
        if (atSign != NULL)
        {
            *atSign = 0 ;
            if (strcmp(resourceString, "mandatory") == 0)
            {
                mandatory = TRUE ;
            }
            *atSign = '@' ;
            parameters = atSign + 1 ;
        }
        else 
        {
            parameters = resourceString ;
        }
    }
    retCode = newApocSource(mandatory, parameters, aError) ;
    if (retCode->mDisconnected) 
    {   /* Log a message indicating the source is unavailable. */
        if (aError != NULL && *aError != NULL) 
        {
            gconf_log(GCL_WARNING, _("%s Source %s is unavailable: %s"),
                      MODULE, aAddress, (*aError)->message) ;
            g_error_free(*aError) ;
            *aError = NULL ;
        }
        else 
        {
            gconf_log(GCL_WARNING, _("%s Source %s is unavailable."),
                      MODULE, aAddress) ;
        }
    }
    g_free(resourceString) ;
    return retCode ;
}

/**
  Removes the listeners of tree change and destroys the watcher holders. 
  */
static void destroyApocSourceTreeWatchers(ApocSource *aSource) 
{
    GSList *iterator = aSource->mTreeWatchers ;

    if (iterator == NULL) { return ; }
    do
    {
        ApocSourceTreeWatcher *watcher = iterator->data ;

        if (watcher->mNodePath != NULL)
        {
            apocTreeAccessRemoveListener(aSource->mTreeAccess, 
                                         watcher->mTreeListenerId) ;
            destroyApocNodePath(watcher->mNodePath) ;
        }
        g_free(watcher) ;
        iterator = g_slist_next(iterator) ;
    } while (iterator != NULL) ;
    g_slist_free(aSource->mTreeWatchers) ;
}

/**
  Destroys an ApocSource.
  */
void destroyApocSource(ApocSource *aSource)
{
    if (aSource != NULL)
    {
        /* Be careful! Tree watchers must be destroyed first since
           their destruction also makes them unregister the tree
           access listeners, so the tree access must still be valid. */
        if (!aSource->mDisconnected) 
        {
            destroyApocSourceTreeWatchers(aSource) ;
            destroyApocTreeAccess(aSource->mTreeAccess) ;
        }
        destroyApocNodePath(aSource->mReplacementPath) ;
        g_free(aSource) ;
    }
}

/**
  Finds a data tree associated to a node path.
  Only the component part of the path is used, loads the tree
  from the policy API if necessary, cleans up cache if possible
  and returns a reference to the tree.
  */
static const ApocTree *apocSourceFindTree(const ApocSource *aSource, 
                                          const ApocNodePath *aPath,
                                          GError **aError)
{
    g_return_val_if_fail(aSource != NULL && aPath != NULL, NULL) ;
    return apocTreeAccessGetTree(aSource->mTreeAccess, 
                                 aPath, aError) ;
}

/**
  Finds a configuration node associated to a node path.
  First retrieves the tree and then searches through it.
  If the node doesn't exist, a null pointer is returned but
  no error is generated. The node returned is a reference.
  The tree containing the node is also returned so as to
  allow the caller to release it when the node is not needed.
  */
static const ApocNode *apocSourceFindNode(const ApocSource *aSource,
                                          const ApocNodePath *aPath, 
                                          const ApocTree **aTree,
                                          GError **aError)
{
    g_return_val_if_fail(aTree != NULL, NULL) ;
    *aTree = apocSourceFindTree(aSource, aPath, aError) ;
    if (*aTree != NULL)
    {
        const ApocNode *retCode = apocTreeFindNode(*aTree, aPath, aError) ;

        if (retCode != NULL &&
                (!apocNodeIsProperty(retCode) || 
                 aSource->mMandatory == apocNodeIsFinalised(retCode)))
        {
            return retCode ;
        }
    }
    return NULL ;
}

/**
  Finds a node from a GConf key.
  Maps the key to a node path and gets the node.
  */
static const ApocNode *apocSourceGConfKeyToNode(const ApocSource *aSource,
                                                const gchar *aKey,
                                                gboolean aIsDirectory,
                                                const ApocTree **aTree,
                                                GError **aError)
{
    ApocNodePath *path = NULL ;
    const ApocNode *retCode = NULL ;

    path = gconfKeyToApocNodePath(aKey, aIsDirectory, aError) ;
    if (path != NULL)
    {
        retCode = apocSourceFindNode(aSource, path, aTree, aError) ;
        destroyApocNodePath(path) ;
    }
    return retCode ;
}

/**
  Returns the special replacement tree.
  Gets a tree whose properties can be used as keywords in other properties
  to do some placeholding.
  */
static const ApocNode *apocSourceGetReplacements(const ApocSource *aSource,
                                                 const ApocTree **aTree)
{
    return apocSourceFindNode(aSource, aSource->mReplacementPath, aTree, NULL) ;
}

/**
  Returns value for a key. 
  Maps the key information to a component and path combination, 
  retrieves the policy data for the component if necessary, gets the
  value and maps it to a GConfValue.
  */
GConfValue *apocSourceQueryValue(const ApocSource *aSource, const gchar *aKey,
                                 const gchar **aLocales, GError **aError)
{
    GConfValue *retCode = NULL ;
    const ApocNode *node = NULL ;
    const ApocTree *tree = NULL ;

    g_return_val_if_fail(aSource != NULL && aKey != NULL, retCode) ;
    if (aSource->mDisconnected) { return retCode ; }
    node = apocSourceGConfKeyToNode(aSource, aKey, FALSE, &tree, aError) ;
    if (node != NULL) 
    {
        const ApocTree *replacementTree = NULL ;
        const ApocNode *replacements = 
                                apocSourceGetReplacements(aSource,
                                                          &replacementTree) ;

        retCode = apocNodeToGConfValue(node, aLocales, replacements, aError) ;
        apocTreeAccessReleaseTree(aSource->mTreeAccess, replacementTree) ;
    }
    apocTreeAccessReleaseTree(aSource->mTreeAccess, tree) ;
    return retCode ;
}

/**
  Returns meta-information for a key.
  Same stuff as getting the value, then puts the last modification
  in the returned GConfMetaInfo.
  */
GConfMetaInfo *apocSourceQueryMetaInfo(const ApocSource *aSource, 
                                       const gchar *aKey,
                                       GError **aError)
{
    GConfMetaInfo *retCode = NULL ;
    const ApocNode *node = NULL ;
    const ApocTree *tree = NULL ;

    g_return_val_if_fail(aSource != NULL && aKey != NULL, retCode) ;
    if (aSource->mDisconnected) { return retCode ; }
    node = apocSourceGConfKeyToNode(aSource, aKey, FALSE, &tree, aError) ;
    if (node != NULL) 
    {
        retCode = apocNodeToGConfMetaInfo(node, aError) ;
    }
    apocTreeAccessReleaseTree(aSource->mTreeAccess, tree) ;
    return retCode ;
}

/**
  Returns the list of entries in a directory.
  Maps directory information to a component and path combination, 
  retrieves the policy data for the component if necessary and lists
  the properties under the node.
  */
GSList *apocSourceAllEntries(const ApocSource *aSource, const gchar *aDirectory,
                             const gchar **aLocales, GError **aError)
{
    GSList *childNodes = NULL ;
    const ApocNode *node = NULL ;
    const ApocTree *tree = NULL ;
    GSList *retCode = NULL ;

    g_return_val_if_fail(aSource != NULL && aDirectory != NULL, retCode) ;
    if (aSource->mDisconnected) { return retCode ; }
    node = apocSourceGConfKeyToNode(aSource, aDirectory, TRUE, &tree, aError) ;
    if (node != NULL)
    {
        GSList *iterator = NULL ;
        const ApocTree *replacementTree = NULL ;
        const ApocNode *replacements = 
                                apocSourceGetReplacements(aSource,
                                                          &replacementTree) ;
        
        childNodes = apocNodeListProperties(node, aError) ;
        for (iterator = childNodes ; iterator != NULL ; 
                                            iterator = g_slist_next(iterator))
        {
            GConfValue *value = NULL ;
            GError *error = NULL ;
            const ApocNode *childNode = iterator->data ;
            
            if (aSource->mMandatory != apocNodeIsFinalised(childNode))
            {   /* Skip properties which don't match the desired
                   protection status. */
                continue ; 
            }
            value = apocNodeToGConfValue(childNode, aLocales, replacements,
                                         &error) ;
            if (error == NULL)
            {
                if (value != NULL)
                {
                    retCode = g_slist_append(retCode,
                                    gconf_entry_new_nocopy(
                                        g_strdup(apocNodeGetName(childNode)), 
                                        value)) ;
                }
            }
            else
            {   
                gconf_log(GCL_ERR, 
                          _("%s Error mapping node to value: %s"),
                          MODULE, error->message) ;
                g_error_free(error) ;
            }
        }
        apocTreeAccessReleaseTree(aSource->mTreeAccess, replacementTree) ;
    }
    apocTreeAccessReleaseTree(aSource->mTreeAccess, tree) ;
    return retCode ;
}

/**
  Returns the list of subdirectories in a directory.
  Same stuff as listing entries, then the child nodes or child 
  component parts are listed.
  */
GSList *apocSourceAllSubdirs(const ApocSource *aSource, const gchar *aDirectory,
                             GError **aError)
{
    GSList *retCode = NULL ;
    ApocNodePath *path = NULL ;

    g_return_val_if_fail(aSource != NULL && aDirectory != NULL, retCode) ;
    if (aSource->mDisconnected) { return retCode ; }
    path = gconfKeyToApocNodePath(aDirectory, TRUE, aError) ;
    if (path != NULL)
    {
        const ApocTree *tree = NULL ;

        tree = apocSourceFindTree(aSource, path, aError) ;
        if (tree != NULL) 
        {
            retCode = apocTreeFindSubPaths(tree, path, aError) ;
            apocTreeAccessReleaseTree(aSource->mTreeAccess, tree) ;
        }
        destroyApocNodePath(path) ;
    }
    return retCode ;
}

/**
  Tests existence of a directory.
  Same stuff as the listing methods, then just tests for existence.
  */
gboolean apocSourceDirExists(const ApocSource *aSource, const gchar *aDirectory,
                             GError **aError)
{
    gboolean retCode = FALSE ;
    ApocNodePath *path = NULL ;

    g_return_val_if_fail(aSource != NULL && aDirectory != NULL, retCode) ;
    if (aSource->mDisconnected) { return retCode ; }
    path = gconfKeyToApocNodePath(aDirectory, TRUE, aError) ;
    if (path != NULL)
    {
        const ApocTree *tree = apocSourceFindTree(aSource, path, aError) ;

        if (tree != NULL)
        {
            retCode = apocTreeCheckPath(tree, path, aError) ;
            apocTreeAccessReleaseTree(aSource->mTreeAccess, tree) ;
        }
        destroyApocNodePath(path) ;
    }
    return retCode ;
}

/**
  Clears the cache.
  Discards all previously loaded component data.
  */
void apocSourceClearCache(const ApocSource *aSource)
{
}

/**
  Callback function used when a node changes.
  Generates values for both nodes and checks what must be sent to
  the caller who registered the notification.
  */
static void nodeChangeCallback(const ApocNode *aOldNode,
                               const ApocNode *aNewNode,
                               gpointer aContext)
{
    ApocSource *source = aContext ;
    gboolean mandatory = source->mMandatory ;
    gchar *nodeName = NULL ;

    if (aNewNode != NULL)
    {   /* Potential node creation/change */
        if (apocNodeIsFinalised(aNewNode) == mandatory)
        {   /* Yep. */
            nodeName = apocNodeGetFullName(aNewNode) ;
            source->mCallback((GConfSource *) source,
                              nodeName,
                              source->mUserData) ;
        }
        else
        {   /* Mark the node as NULL to look for a possible deletion 
               in the next pass. */
            aNewNode = NULL ; 
        }
    }
    if (aNewNode == NULL)
    {   /* Potential node deletion */
        if (aOldNode != NULL && apocNodeIsFinalised(aOldNode) == mandatory)
        {   /* Right on. */
            nodeName = apocNodeGetFullName(aOldNode) ;
            source->mCallback((GConfSource *) source,
                              nodeName,
                              source->mUserData) ;
        }
    }
    if (nodeName != NULL) { g_free(nodeName) ; }
}

/**
  Callback function used when a tree changes. 
  Compares the nodes from the node path stored in the notification
  down and generates a notification to the caller for each of these
  events.
  */
static void treeChangeCallback(const ApocTree *aOldTree,
                               const ApocTree *aNewTree,
                               gpointer aContext)
{
    ApocSourceTreeWatcher *watcher = aContext ;
    const ApocNode *oldNode = NULL ;
    const ApocNode *newNode = NULL ;

    g_return_if_fail(aContext != NULL && aOldTree != NULL && aNewTree != NULL) ;
    oldNode = apocTreeFindNode(aOldTree, watcher->mNodePath, NULL) ;
    newNode = apocTreeFindNode(aNewTree, watcher->mNodePath, NULL) ;
    if (oldNode != NULL || newNode != NULL)
    {
        apocNodeCompare(oldNode, newNode, 
                        nodeChangeCallback, 
                        (gpointer) (watcher->mSource)) ;
    }
}

/**
  Collection callback function used to gather all root node paths
  for which client listeners exist and create the corresponding tree
  watchers.
  */
static void buildTreeWatchers(const gchar *aKey,
                              guint aIgnored,
                              gpointer aIgnoredToo,
                              gpointer aContext) 
{
    g_return_if_fail(aKey != NULL && aContext != NULL) ;
    apocSourceNewListener(aContext, aKey) ;
}

/**
  Adds a tree watcher in a source.
  Checks that it can register a listener with the tree access manager
  and stores the watcher structure in the list.
  Will take over the node path passed (either stored or destroyed).
  */
static void apocSourceAddTreeWatcher(ApocSource *aSource, 
                                     ApocNodePath *aPath,
                                     GSList *aWhere)
{
    GError *error = NULL ; 
    ApocSourceTreeWatcher *watcher = NULL ;

    g_return_if_fail(aSource != NULL) ;
    watcher = g_new0(ApocSourceTreeWatcher, 1) ;
    watcher->mTreeListenerId = apocTreeAccessAddListener(aSource->mTreeAccess,
                                                         aPath,
                                                         treeChangeCallback,
                                                         watcher, 
                                                         &error) ;
    if (watcher->mTreeListenerId > 0) 
    {
        watcher->mNodePath = aPath ;
        watcher->mSource = aSource ;
        if (aWhere != NULL)
        {
            aSource->mTreeWatchers = 
                                 g_slist_insert_before(aSource->mTreeWatchers, 
                                                       aWhere,
                                                       watcher) ;
        }
        else 
        {
            aSource->mTreeWatchers = g_slist_append(aSource->mTreeWatchers,
                                                    watcher) ;
        }
    }
    else
    {
        if (error != NULL)
        {
            gconf_log(GCL_ERR, 
                      _("%s Couldn't register listener: %s"),
                      MODULE, error->message) ;
            g_error_free(error) ;
        }
        else
        {
            gconf_log(GCL_WARNING, 
                      _("%s Couldn't register listener."), MODULE) ;
        }
        destroyApocNodePath(aPath) ;
        g_free(watcher) ;
    }
}

/**
  Initialises the tree watchers from the client listeners list.
  Goes through all the client listeners and retrieves a list of
  unique node paths for which to get tree listeners.
  */
void apocSourceInitListeners(ApocSource *aSource,
                             GConfSourceNotifyFunc aCallback,
                             gpointer aUserData)
{
    g_return_if_fail(aSource != NULL && aCallback != NULL) ;
    if (aSource->mDisconnected) { return ; }
    aSource->mCallback = aCallback ;
    aSource->mUserData = aUserData ;
    apocSourceNewListener(aSource, "/") ;
}

/**
  Notifies that a new client listener has been added.
  Checks whether that listener maps to a previously unlistened to tree
  and if so adds a tree watcher to the list.
  */
void apocSourceNewListener(ApocSource *aSource, const gchar *aLocation) 
{
    ApocNodePath *path = NULL ;
    GError *error = NULL ;
    
    g_return_if_fail(aSource != NULL && aLocation != NULL) ;
    if (aSource->mDisconnected) { return ; }
    path = gconfKeyToApocNodePath(aLocation, TRUE, &error) ;
    if (error == NULL)
    {
        GSList *iterator = aSource->mTreeWatchers ;
        const gchar *component = apocNodePathGetComponent(path) ;

        while (iterator != NULL) 
        {
            ApocSourceTreeWatcher *watcher = iterator->data ;
            gint difference = 
                            strcmp(apocNodePathGetComponent(watcher->mNodePath),
                                   component) ;
             
            if (difference > 0)
            {
                break ;
            }
            if (difference == 0) 
            {   /* We've found a watcher for that tree already, we
                   need to check if we should change the point of
                   watching within the tree. */
                ApocNodePath *commonPath = NULL ;

                commonPath = apocNodePathGetCommonPath(path, 
                                                       watcher->mNodePath) ;
                if (path != commonPath) { destroyApocNodePath(path) ; }
                if (watcher->mNodePath != commonPath) 
                {
                    destroyApocNodePath(watcher->mNodePath) ;
                    watcher->mNodePath = commonPath ;
                }
                return ;
            }
            iterator = g_slist_next(iterator) ;
        }
        apocSourceAddTreeWatcher(aSource, path, iterator) ;
    }
    else 
    {
        gconf_log(GCL_ERR, 
                  _("%s Couldn't convert key '%s' to node path: %s"),
                  MODULE, aLocation, error->message) ;
    }
}


