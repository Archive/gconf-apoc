/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either
 * the GNU General Public License Version 2 only ("GPL") or
 * the Common Development and Distribution License("CDDL")
 * (collectively, the "License"). You may not use this file
 * except in compliance with the License. You can obtain a copy
 * of the License at www.sun.com/CDDL or at COPYRIGHT. See the
 * License for the specific language governing permissions and
 * limitations under the License. When distributing the software,
 * include this License Header Notice in each file and include
 * the License file at /legal/license.txt. If applicable, add the
 * following below the License Header, with the fields enclosed
 * by brackets [] replaced by your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by
 * only the CDDL or only the GPL Version 2, indicate your
 * decision by adding "[Contributor] elects to include this
 * software in this distribution under the [CDDL or GPL
 * Version 2] license." If you don't indicate a single choice
 * of license, a recipient has the option to distribute your
 * version of this file under either the CDDL, the GPL Version
 * 2 or to extend the choice of license to its licensees as
 * provided above. However, if you add GPL Version 2 code and
 * therefore, elected the GPL Version 2 license, then the
 * option applies only if the new code is made subject to such
 * option by the copyright holder.
 */


#include <gconf/gconf.h>
#include <gmodule.h>

#include "common-utils.h"
#include "gconf-backend.h"
#include "apoc-source.h"

/**
  General front-end of the Apoc backend. The backend provides policy data
  accessed through a policy access API. The API provides an ordered list
  of layers containing XML data which have to be merged together in order
  to get the actual configuration data.
  */

static const gchar *MODULE =  "[ApocBackend]" ;

/**
  Shutdown of the backend. Doesn't do anything but log exit.
  */
static void backendShutdown(GError **aError)
{
    gconf_log(GCL_DEBUG, "%s Shutdown.", MODULE) ;
}

/**
  Initial retrieval of a GConfSource. 
  */
static GConfSource *resolveAddress(const gchar *aAddress, GError **aError)
{
    return (GConfSource *) createApocSource(aAddress, aError) ;
}

/**
  Thread-lock method. Not needed here, resource locking should be handled
  by the policy API.
  */
static void lock(GConfSource *aSource, GError **aError)
{
}

/**
  See lock.
  */
static void unlock(GConfSource *aSource, GError **aError)
{
}

/**
  All keys present are readable (not much point in having them there
  otherwise...). 
  */
static gboolean readable(GConfSource *aSource, const gchar *aKey,
                         GError **aError)
{
    return TRUE ;
}

/**
  No key is writable. The policy data is readonly for the clients.
  */
static gboolean writable(GConfSource *aSource, const gchar *aKey,
                         GError **aError)
{
    return FALSE ;
}

/**
  Provision of a value for a given key. No schema data is stored 
  in the policy layers.
  */
static GConfValue *queryValue(GConfSource *aSource, const gchar *aKey,
                              const gchar **aLocales, gchar **aSchema,
                              GError **aError)
{
    if (aSchema != NULL) { *aSchema = NULL ; }
    return apocSourceQueryValue((ApocSource *) aSource, aKey, 
                                aLocales, aError) ;
}

/**
  Provision of meta-information for a given key. 
  */
static GConfMetaInfo *queryMetaInfo(GConfSource *aSource, const gchar *aKey,
                                    GError **aError)
{
    return apocSourceQueryMetaInfo((ApocSource *) aSource, aKey, aError) ;
}

/**
  General purpose method filling the GError for methods which aren't
  supported by the Apoc backend (mostly trying to write anything).
  */
static void notSupported(GError **aError)
{
    gconf_set_error(aError, GCONF_ERROR_NO_PERMISSION,
                    _("%s Unsupported operation."), MODULE) ;
}

/**
  Setting a key value fails, as the policy data is readonly.
  */
static void setValue(GConfSource *aSource, const gchar *aKey, 
                     const GConfValue *aValue, GError **aError)
{
    notSupported(aError) ;
}

/**
  Returns all the entries as a list of GConfEntry with relative paths.
  */
static GSList *allEntries(GConfSource *aSource, const gchar *aDirectory,
                          const gchar **aLocales, GError **aError)
{
    return apocSourceAllEntries((ApocSource *) aSource, aDirectory, 
                                aLocales, aError) ;
}

/**
  Returns all the subdirectories as a list of allocated strings.
  */
static GSList *allSubdirs(GConfSource *aSource, const gchar *aDirectory,
                          GError **aError)
{
    return apocSourceAllSubdirs((ApocSource *) aSource, aDirectory, aError) ;
}

/**
  Unsetting a key value fails, as the policy data is readonly.
  */
static void unsetValue(GConfSource *aSource, const gchar *aKey, 
                       const gchar *aLocale, GError **aError)
{
    notSupported(aError) ;
}

/**
  Tests whether a given directory exists.
  */
static gboolean dirExists(GConfSource *aSource, const gchar *aDirectory,
                          GError **aError)
{
    return apocSourceDirExists((ApocSource *) aSource, aDirectory, aError) ;
}

/**
  Removing a directory fails, as the policy data is readonly.
  */
static void removeDir(GConfSource *aSource, const gchar *aDirectory,
                      GError **aError)
{
    notSupported(aError) ;
}

/**
  Setting a key schema fails, as the policy data doesn't hold any
  schema information and is readonly.
  */
static void setSchema(GConfSource *aSource, const gchar *aKey, 
                      const gchar *aSchema, GError **aError)
{
    notSupported(aError) ;
}

/**
  No syncing necessary as no write allowed.
  */
static gboolean syncAll(GConfSource *aSource, GError **aError)
{
    return TRUE ;
}

/**
  Gets rid of the source.
  */
static void destroySource(GConfSource *aSource)
{
    destroyApocSource((ApocSource *) aSource) ;
}

/**
  Nukes cached data.
  */
static void clearCache(GConfSource *aSource)
{
    apocSourceClearCache((ApocSource *) aSource) ;
}

/**
  Probably does something in some other backend...
  */
static void blowAwayLocks(const gchar *aAddress)
{
}

/**
  Sets the notification function.
  */
static void setNotificationCallback(GConfSource *aSource, 
                                    GConfSourceNotifyFunc aCallback,
                                    gpointer aUserData)
{
    apocSourceInitListeners((ApocSource *) aSource, aCallback, aUserData) ;
}

/**
  Tells the source a new location has a listener.
  */
static void addListener(GConfSource *aSource, guint aId, const gchar *aLocation)
{
    /*apocSourceNewListener((ApocSource *) aSource, aLocation) ;*/
}

/**
  Removes a previously added listener.
  */
static void removeListener(GConfSource *aSource, guint aId)
{
}

/**
  The virtual table of methods expected from a backend.
  */
static GConfBackendVTable kApocVTable = 
{
    sizeof(GConfBackendVTable),
    backendShutdown,
    resolveAddress,
    lock,
    unlock,
    readable,
    writable,
    queryValue,
    queryMetaInfo,
    setValue,
    allEntries,
    allSubdirs,
    unsetValue,
    dirExists,
    removeDir,
    setSchema,
    syncAll,
    destroySource,
    clearCache,
    blowAwayLocks,
    setNotificationCallback,
    addListener,
    removeListener
} ;

/**
  Initialiser. Doesn't do anything but log load.
  */
G_MODULE_EXPORT const gchar *g_module_check_init(GModule *aModule)
{
    gconf_log(GCL_DEBUG, "%s Initialised.", MODULE) ;
    return NULL ;
}

/**
  Returns the virtual table for the backend.
  */
G_MODULE_EXPORT GConfBackendVTable *gconf_backend_get_vtable(void)
{
    return &kApocVTable ;
}


