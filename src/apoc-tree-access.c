/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either
 * the GNU General Public License Version 2 only ("GPL") or
 * the Common Development and Distribution License("CDDL")
 * (collectively, the "License"). You may not use this file
 * except in compliance with the License. You can obtain a copy
 * of the License at www.sun.com/CDDL or at COPYRIGHT. See the
 * License for the specific language governing permissions and
 * limitations under the License. When distributing the software,
 * include this License Header Notice in each file and include
 * the License file at /legal/license.txt. If applicable, add the
 * following below the License Header, with the fields enclosed
 * by brackets [] replaced by your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by
 * only the CDDL or only the GPL Version 2, indicate your
 * decision by adding "[Contributor] elects to include this
 * software in this distribution under the [CDDL or GPL
 * Version 2] license." If you don't indicate a single choice
 * of license, a recipient has the option to distribute your
 * version of this file under either the CDDL, the GPL Version
 * 2 or to extend the choice of license to its licensees as
 * provided above. However, if you add GPL Version 2 code and
 * therefore, elected the GPL Version 2 license, then the
 * option applies only if the new code is made subject to such
 * option by the copyright holder.
 */


#include "apoc-tree-access.h"
#include "common-utils.h"

#include <pthread.h>
#include <string.h>

static const gchar *MODULE =  "[ApocTreeAccess]" ;

/**
  Structure holding a tree and its associated listeners.
  This structure is held in a hashtable by the tree access, the key
  being the component name (name of the tree).
  */
typedef struct
{
    ApocTree *mTree ;               /** The actual current tree. */
    pthread_mutex_t mTreeLock ;    /** Protection for access to the tree. */
    PAPIListenerId mListenerId ;    /** Listener ID obtained from the PAPI */
    GArray *mListeners ;            /** Array of integers, point to the general
                                        listeners list. */
} ApocTreeHolder ;

/**
  Constructor of ApocTreeHolder.
  */
static ApocTreeHolder *createApocTreeHolder(ApocTree *aTree)
{
    ApocTreeHolder *retCode = NULL ;

    g_return_val_if_fail(aTree != NULL, retCode) ;
    retCode = g_new0(ApocTreeHolder, 1) ;
    retCode->mTree = aTree ;
    pthread_mutex_init(&(retCode->mTreeLock), 0) ;
    return retCode ;
}

/**
  Destructor of ApocTreeHolder.
  */
static void destroyApocTreeHolder(ApocTreeHolder *aHolder)
{
    if (aHolder != NULL)
    {
        pthread_mutex_destroy(&(aHolder->mTreeLock)) ;
        g_free(aHolder) ;
    }
}

/**
  Structure holding a callback/context pair to be used when
  generating notifications in case of a tree change.
  */
typedef struct
{
    gchar *mTreeName ; 
    ApocTreeChangeCallback mCallback ;
    gpointer mUserData ;
} ApocTreeListener ;

/**
  Structure handling the access to the configuration trees.
  Holds the tree cache and the connection to the PAPI.
  */
struct privApocTreeAccess
{
    GHashTable *mTrees ;                /** Component to tree cache. */
    ApocPapi *mPapi ;                   /** PAPI functions.*/
    ApocPapiConnection *mConnection ;   /** PAPI connection. */
    PAPIListenerId mGlobalListener ;    /** Global PAPI listener for all
                                            components. */
    gint mRefCount ;                    /** Sharing reference count. */
    GArray *mListeners ;                /** Array of callback/context pairs. */
    GArray *mSpecialListeners ;         /** Array of integers pointing into the
                                            main notification array for the 
                                            special notifications. */
} ;

/**
  The tree accessor is here handled as a singleton. 
  Since there are no meaningful parameters which could distinguish
  one instance from another, we might as well always return the same
  instance and benefit from a shared cache and connection to the PAPI.
  If parameters become used, some hashing from meaningful parameters
  values to instances of tree access objects will be necessary in order
  to maximise configuration data sharing.
  */
static ApocTreeAccess *kInstance = NULL ;

/** Forward declaration of the refresh method. */
static void apocTreeAccessRefreshTree(const ApocTreeAccess *aTreeAccess,
                                      ApocTreeHolder *aTree) ;
/**
  Forward declaration of the test method to check if the tree accessor
  is being destroyed or not.
  */
static gboolean apocTreeAccessShuttingDown(const ApocTreeAccess *aTreeAccess) ;
/**
  Callback function used when the PAPI notifies a change for a component.
  */
static void treeChangeListener(const PAPIEvent *aEvent, gpointer aTreeAccess)
{
    ApocTreeAccess *treeAccess = aTreeAccess ;
    ApocTreeHolder *tree = NULL ;

    g_return_if_fail(aEvent != NULL && 
                     aEvent->mComponent != NULL && 
                     treeAccess != NULL) ;
    if (apocTreeAccessShuttingDown(treeAccess)) { return ; }
    tree = g_hash_table_lookup(treeAccess->mTrees, aEvent->mComponent) ;
    if (tree == NULL) 
    {
        gconf_log(GCL_WARNING, 
                  _("%s Received notification for non-existent tree (%s)."), 
                  MODULE, aEvent->mComponent) ;
        return ; 
    }
    apocTreeAccessRefreshTree(treeAccess, tree) ;
}

/**
  Callback function to destroy a tree/notification cache element.
  */
static void destroyCacheElement(gpointer aName, 
                                gpointer aTreeHolder, 
                                gpointer aTreeAccess)
{
    ApocTreeHolder *treeHolder = aTreeHolder ;
    ApocTreeAccess *treeAccess = aTreeAccess ;

    if (treeHolder->mListenerId != NULL)
    {
        if (!treeAccess->mConnection->mDisconnected) 
        {
            treeAccess->mPapi->mRemoveListener(
                                            treeAccess->mConnection->mContext,
                                            treeHolder->mListenerId,
                                            NULL) ;
        }
    }
    if (treeHolder->mListeners != NULL)
    {
        g_array_free(treeHolder->mListeners, TRUE) ;
    }
    pthread_mutex_lock(&(treeHolder->mTreeLock)) ;
    destroyApocTree(treeHolder->mTree) ;
    treeHolder->mTree = NULL ;
    pthread_mutex_unlock(&(treeHolder->mTreeLock)) ;
    destroyApocTreeHolder(treeHolder) ;
}

/**
  Destructor of a potentially shared Apoc tree access object.
  */
static void deleteApocTreeAccess(ApocTreeAccess *aTreeAccess) 
{
    if (aTreeAccess->mConnection != NULL)
    {
        if (aTreeAccess->mGlobalListener != NULL && 
                !aTreeAccess->mConnection->mDisconnected)
        {
            aTreeAccess->mPapi->mRemoveListener(
                                            aTreeAccess->mConnection->mContext,
                                            aTreeAccess->mGlobalListener,
                                            NULL) ;
        }
        if (aTreeAccess->mTrees != NULL)
        {
            g_hash_table_foreach(aTreeAccess->mTrees, destroyCacheElement, 
                                 aTreeAccess) ;
            g_hash_table_destroy(aTreeAccess->mTrees) ;
        }
        destroyApocPapiConnection(aTreeAccess->mPapi, 
                                  aTreeAccess->mConnection) ;
    }
    if (aTreeAccess->mPapi != NULL)
    {
        destroyApocPapi(aTreeAccess->mPapi) ;
    }
    /* The notification destructions were performed by the trees 
       they were associated to. */
    if (aTreeAccess->mListeners != NULL) 
    {
        guint i = 0 ;

        for (i = 0 ; i < aTreeAccess->mListeners->len ; ++ i)
        {
            g_free(g_array_index(aTreeAccess->mListeners, 
                                 ApocTreeListener, i).mTreeName) ;
        }
        g_array_free(aTreeAccess->mListeners, TRUE) ;
    }
    g_free(aTreeAccess) ;
}

/**
  Visitor of the tree hashtable triggering a reload on each item.
  */
static void refreshTree(gpointer aComponent, 
                        gpointer aTree, 
                        gpointer aTreeAccess)
{
    apocTreeAccessRefreshTree(aTreeAccess, aTree) ;
}

/**
  Registers a global listener for all possible trees.
  */
static void apocTreeAccessRegisterGlobalListener(ApocTreeAccess *aTreeAccess)
{
    PAPIStatus status = PAPISuccess ;

    g_return_if_fail(aTreeAccess != NULL) ;
    if (aTreeAccess->mConnection == NULL ||
            aTreeAccess->mConnection->mDisconnected) { return ; }
    aTreeAccess->mGlobalListener = 
        aTreeAccess->mPapi->mAddListener(aTreeAccess->mConnection->mContext,
                                         apocNodePathGetCommonPrefix(),
                                         treeChangeListener,
                                         aTreeAccess,
                                         &status) ;
    if (status != PAPISuccess) 
    {
        gconf_log(GCL_WARNING, _("%s Cannot add listener for %s to PAPI: %s"),
                  MODULE, apocNodePathGetCommonPrefix(), 
                  apocPapiGetError(status)) ;
    }
}

/**
  Callback for PAPI connection status change.
  */
static void papiConnectionCallback(PAPIConnectionState aState,
                                   gpointer aTreeAccess)
{
    ApocTreeAccess *treeAccess = aTreeAccess ;

    treeAccess->mConnection->mDisconnected = (aState == PAPIDisconnected) ;
    if (apocTreeAccessShuttingDown(treeAccess)) { return ; }
    if (!treeAccess->mConnection->mDisconnected) 
    {   /** Connection's back, if needed we register the global listener
            and we trigger a cycle of tree reloads. */
        if (treeAccess->mGlobalListener == NULL)
        {
            apocTreeAccessRegisterGlobalListener(treeAccess) ;
        }
        g_hash_table_foreach(treeAccess->mTrees, refreshTree, treeAccess) ;
    }
    gconf_log(GCL_DEBUG, "%s PAPI is now %s.", 
              MODULE, treeAccess->mConnection->mDisconnected ? "disconnected" :
                                                               "connected") ;
}

/**
  Constructor of a potentially shared Apoc tree access object.
  */
static ApocTreeAccess *newApocTreeAccess(const gchar *aParameters, 
                                         GError **aError)
{
    ApocTreeAccess *retCode = g_new0(ApocTreeAccess, 1) ;

    retCode->mPapi = createApocPapi(aError) ;
    if (retCode->mPapi != NULL) 
    {
        retCode->mConnection = createApocPapiConnection(retCode->mPapi,
                                                        papiConnectionCallback,
                                                        retCode,
                                                        aError) ;
        if (retCode->mConnection == NULL) 
        {
            deleteApocTreeAccess(retCode) ;
            retCode = NULL ;
        }
        else 
        {
            apocTreeAccessRegisterGlobalListener(retCode) ;
        }
    }
    else
    {
        deleteApocTreeAccess(retCode) ;
        retCode = NULL ;
    }
    return retCode ;
}

/**
  Acquisition function for an Apoc tree access object.
  */
static gint apocTreeAccessAcquire(ApocTreeAccess *aTreeAccess)
{
    if (aTreeAccess != NULL)
    {
        return ++ aTreeAccess->mRefCount ;
    }
    return -1 ;
}

/**
  Exposed constructor.
  Builds if necessary and returns the singleton instance.
  */
ApocTreeAccess *createApocTreeAccess(const gchar *aParameters, 
                                     GError **aError)
{
    if (kInstance == NULL)
    {
        kInstance = newApocTreeAccess(aParameters, aError) ;
    }
    apocTreeAccessAcquire(kInstance) ;
    return kInstance ;
}

/**
  Release function for an Apoc tree access object.
  */
static gint apocTreeAccessRelease(ApocTreeAccess *aTreeAccess)
{
    if (aTreeAccess != NULL)
    {
        return -- aTreeAccess->mRefCount ;
    }
    return -1 ;
}

/**
  Checks whether the tree access object is being destroyed, which is deducted
  from its ref count being 0.
  */
static gboolean apocTreeAccessShuttingDown(const ApocTreeAccess *aTreeAccess)
{
    return aTreeAccess == NULL || aTreeAccess->mRefCount == 0 ;
}

/**
  Destructor.
  Controls we are dealing with the singleton instance, dereferences it
  and destroys it if necessary.
  */
void destroyApocTreeAccess(ApocTreeAccess *aTreeAccess)
{
    g_return_if_fail(aTreeAccess == kInstance) ;
    if (apocTreeAccessRelease(kInstance) == 0)
    {
        deleteApocTreeAccess(kInstance) ;
    }
}

/**
  Reloads the contents of a tree and sends notifications to its listeners.
  */
static void apocTreeAccessRefreshTree(const ApocTreeAccess *aTreeAccess,
                                      ApocTreeHolder *aTree) 
{
    ApocTree *oldTree = NULL ;
    guint i = 0 ;

    pthread_mutex_lock(&(aTree->mTreeLock)) ;
    oldTree = aTree->mTree ;
    aTree->mTree = cloneApocTree(oldTree, NULL) ;
    pthread_mutex_unlock(&(aTree->mTreeLock)) ;
    if (aTree->mListeners != NULL) 
    {
        for (i = 0 ; i < aTree->mListeners->len ; ++ i)
        {
            ApocTreeListener *listener = NULL ;
            guint listenerId = g_array_index(aTree->mListeners, guint, i) ;

            if (listenerId == 0) { continue ; }
            -- listenerId ;
            listener = &g_array_index(aTreeAccess->mListeners,
                                      ApocTreeListener, listenerId) ;
            if (listener->mCallback != NULL)
            {
                listener->mCallback(oldTree, aTree->mTree, 
                                    listener->mUserData) ;
            }
        }
    }
    destroyApocTree(oldTree) ;
}

/**
  Adds a listener to a tree holder.
  */
static void apocTreeHolderAddListener(ApocTreeHolder *aHolder,
                                      const ApocTreeAccess *aTreeAccess,
                                      guint aListenerId,
                                      GError **aError)
{
    g_return_if_fail(aHolder != NULL && aTreeAccess != NULL && 
                     aListenerId > 0) ;
    if (aHolder->mListeners == NULL)
    {
        aHolder->mListeners = g_array_new(FALSE, TRUE, sizeof(guint)) ;
    }
    g_array_append_val(aHolder->mListeners, aListenerId) ;
}

/**
  Checks if a newly created tree needs to be added a PAPI listener
  on behalf of a special listener which should contain it.
  */
static void apocTreeAccessCheckSpecialListeners(
                                            const ApocTreeAccess *aTreeAccess,
                                            ApocTreeHolder *aTree,
                                            GError **aError)
{
    guint i = 0 ;
    
    g_return_if_fail(aTreeAccess != NULL && aTree != NULL) ;
    if (aTreeAccess->mSpecialListeners == NULL) { return ; }
    for (i = 0 ; i < aTreeAccess->mSpecialListeners->len ; ++ i)
    {
        guint listenerId = 0 ;
        ApocTreeListener *listener = NULL ;

        listenerId = g_array_index(aTreeAccess->mSpecialListeners,
                                   guint, i) ;
        listener = &g_array_index(aTreeAccess->mListeners, 
                                  ApocTreeListener,
                                  listenerId - 1) ;
        if (apocNodePathIsTree(listener->mTreeName, 
                               apocTreeGetName(aTree->mTree)))
        {
            apocTreeHolderAddListener(aTree, aTreeAccess,
                                      listenerId, aError) ;
            gconf_log(GCL_DEBUG, 
                      "%s Added belated special listener for %s (%d).",
                      MODULE, apocTreeGetName(aTree->mTree), listenerId) ;
        }
    }
}

/**
  Gets the holder structure for a tree. If it doesn't exist, the tree 
  is created and a new structure is generated to hold it.
  */
static ApocTreeHolder *apocTreeAccessGetTreeHolder(
                                            const ApocTreeAccess *aTreeAccess, 
                                            const ApocNodePath *aPath, 
                                            GError **aError)
{
    ApocTreeHolder *retCode = NULL ;
    
    g_return_val_if_fail(aTreeAccess != NULL && aPath != NULL, retCode) ;
    if (aTreeAccess->mTrees != NULL)
    {
        retCode = g_hash_table_lookup(aTreeAccess->mTrees, 
                                      apocNodePathGetComponent(aPath)) ;
    }
    if (retCode == NULL)
    {
        ApocTree *tree = createApocTree(aPath, aTreeAccess->mPapi, 
                                        aTreeAccess->mConnection, aError) ;

        if (tree != NULL)
        {
            const gchar *commonPrefix = apocNodePathGetCommonPrefix() ;
            
            retCode = createApocTreeHolder(tree) ;
            if (strncmp(apocTreeGetName(tree), commonPrefix, 
                                                    strlen(commonPrefix)) != 0)
            { /* Tree not managed by global listener. */
                PAPIStatus status = PAPISuccess ;
                
                retCode->mListenerId = 
                    aTreeAccess->mPapi->mAddListener(
                                            aTreeAccess->mConnection->mContext,
                                            apocTreeGetName(tree),
                                            treeChangeListener,
                                            (void *) aTreeAccess,
                                            &status) ;
            }
            if (aTreeAccess->mTrees == NULL)
            {
                ((ApocTreeAccess *) aTreeAccess)->mTrees = 
                                    g_hash_table_new(g_str_hash, g_str_equal) ;
            }
            g_hash_table_insert(aTreeAccess->mTrees, 
                                (gpointer) apocTreeGetName(tree),
                                retCode) ;
            apocTreeAccessCheckSpecialListeners(aTreeAccess, 
                                                retCode, aError) ;
        }
    }
    return retCode ;
}

/**
  Retrieves a configuration data tree.
  If present in the cache, get it from there, if not, get it from 
  the PAPI and store it in the cache. Finally get the lock from the
  holder structure to prevent that tree from being destroyed while
  it's being accessed.
  */
const ApocTree *apocTreeAccessGetTree(const ApocTreeAccess *aTreeAccess,
                                      const ApocNodePath *aPath, 
                                      GError **aError)
{
    const ApocTree *retCode = NULL ;
    ApocTreeHolder *holder = apocTreeAccessGetTreeHolder(aTreeAccess,
                                                         aPath,
                                                         aError) ;

    if (holder != NULL) 
    {
        pthread_mutex_lock(&(holder->mTreeLock)) ;
        retCode = holder->mTree ;
    }
    return retCode ;
}

/**
  Releases a tree obtained by GetTree.
  */
void apocTreeAccessReleaseTree(const ApocTreeAccess *aTreeAccess,
                               const ApocTree *aTree)
{
    ApocTreeHolder *holder = NULL ;
    
    g_return_if_fail(aTreeAccess != NULL && 
                     aTree != NULL && 
                     aTreeAccess->mTrees != NULL) ;
    holder = g_hash_table_lookup(aTreeAccess->mTrees, apocTreeGetName(aTree)) ;
    if (holder != NULL) 
    {
        pthread_mutex_unlock(&(holder->mTreeLock)) ;
    }
}

/**
  Creates a callback structure in the notifications array of the
  tree access and returns its index there plus one, in order to 
  keep 0 as a neutral value. 
  */
static guint apocTreeAccessCreateListener(ApocTreeAccess *aTreeAccess,
                                          const gchar *aTreeName,
                                          ApocTreeChangeCallback aCallback,
                                          gpointer aUserData,
                                          GError ** aError)
{
    guint retCode = 0 ;
    ApocTreeListener *newListener = NULL ;
    
    g_return_val_if_fail(aTreeAccess != NULL && aTreeName != NULL && 
                         aCallback != NULL, retCode) ;
    if (aTreeAccess->mListeners == NULL)
    {
        aTreeAccess->mListeners = g_array_new(FALSE, TRUE, 
                                              sizeof(ApocTreeListener)) ;
    }
    for (retCode = 0 ; retCode < aTreeAccess->mListeners->len ; ++ retCode)
    {
        if (g_array_index(aTreeAccess->mListeners, 
                          ApocTreeListener, 
                          retCode).mTreeName == NULL)
        {
            break ;
        }
    }
    if (retCode == aTreeAccess->mListeners->len)
    {
        g_array_set_size(aTreeAccess->mListeners, retCode + 1) ;
    }
    newListener = &g_array_index(aTreeAccess->mListeners, 
                                 ApocTreeListener, retCode) ;
    newListener->mTreeName = g_strdup(aTreeName) ;
    newListener->mCallback = aCallback ;
    newListener->mUserData = aUserData ;
    return retCode + 1 ;
}

/**
  Adds a special listener pointing to an existing listener structure.
  */
static void apocTreeAccessAddSpecialListener(ApocTreeAccess *aTreeAccess,
                                             guint aListenerId)
{
    g_return_if_fail(aTreeAccess != NULL && aListenerId != 0) ;
    if (aTreeAccess->mSpecialListeners == NULL)
    {
        aTreeAccess->mSpecialListeners = g_array_new(FALSE, TRUE,
                                                     sizeof(guint)) ;
    }
    g_array_append_val(aTreeAccess->mSpecialListeners, aListenerId) ;
}
   
/** Context for tree table traversal to add special listener to tree */
typedef struct 
{
    const ApocNodePath *mPath ;   /** Parent node path */
    ApocTreeAccess *mTreeAccess ; /** Tree access handler */
    guint mListenerId ;           /** Special notification to add on matches */
} ApocListenerAdditionContext ;

/**
  Callback function used when visiting existing trees to check if they
  should be added a listener on behalf of a special one.
  We check if the tree's component name is a subcomponent of the 
  parent one passed, and if so the listener passed is added to the
  tree's list.
  */
static void addListenerToSubTrees(gpointer aName, gpointer aTree,
                                  gpointer aContext)
{
    ApocListenerAdditionContext *context = aContext ;
    ApocTreeHolder *tree = aTree ;
    const gchar *name = aName ;
    GError *error = NULL ;
    
    g_return_if_fail(aName != NULL && aTree != NULL && aContext != NULL) ;
    if (apocNodePathIsTree(apocNodePathGetComponent(context->mPath), name))
    {
        apocTreeHolderAddListener(tree, context->mTreeAccess,
                                  context->mListenerId, &error) ;
        if (error != NULL)
        {
            gconf_log(GCL_ERR, 
                      _("%s Error registering listener for %s: %s"), 
                      MODULE, 
                      name,
                      error->message) ;
            g_error_free(error) ;
        }
        gconf_log(GCL_DEBUG, "%s Added subtree listener for %s (%d).",
                  MODULE, name, context->mListenerId) ;
    }
}

/**
  Adds a listener to the tree access.
  */
guint apocTreeAccessAddListener(ApocTreeAccess *aTreeAccess,
                                const ApocNodePath *aPath,
                                ApocTreeChangeCallback aCallback,
                                gpointer aUserData,
                                GError **aError)
{
    guint retCode = 0 ;
    
    g_return_val_if_fail(aTreeAccess != NULL && aPath != NULL && 
                         aCallback != NULL, retCode) ;
    retCode = apocTreeAccessCreateListener(aTreeAccess, 
                                           apocNodePathGetComponent(aPath),
                                           aCallback, aUserData, aError) ;
    if (retCode > 0) 
    {
        /* Retrieve or create tree for the path */
        ApocTreeHolder *holder = apocTreeAccessGetTreeHolder(aTreeAccess, 
                                                             aPath, 
                                                             aError) ;

        if (holder != NULL)
        {
            apocTreeHolderAddListener(holder, aTreeAccess, 
                                      retCode, aError) ;
            gconf_log(GCL_DEBUG, "%s Added listener for %s (%d).",
                      MODULE, apocNodePathGetComponent(aPath), retCode) ;
        }
        if (apocNodePathIsSpecial(aPath))
        {   /* Need to go through existing sub trees and remember
               the listener for future additions */
            ApocListenerAdditionContext context ;

            context.mPath = aPath ;
            context.mTreeAccess = aTreeAccess ;
            context.mListenerId = retCode ;
            g_hash_table_foreach(aTreeAccess->mTrees,
                                 addListenerToSubTrees,
                                 &context) ;
            apocTreeAccessAddSpecialListener(aTreeAccess, retCode) ;
            gconf_log(GCL_DEBUG, 
                      "%s Added special listener for %s (%d).",
                      MODULE, apocNodePathGetComponent(aPath), retCode) ;
        }
    }
    return retCode ;
}

/**
  Removes a change detection listener added by the previous function.
  */
void apocTreeAccessRemoveListener(ApocTreeAccess *aTreeAccess,
                                  guint aListenerId)
{
    ApocTreeListener *listener = NULL ;
    ApocTreeHolder *holder = NULL ;
    guint i = 0 ;
    
    g_return_if_fail(aTreeAccess != NULL && 
                     aTreeAccess->mListeners != NULL && 
                     aListenerId > 0) ;
    listener = &g_array_index(aTreeAccess->mListeners, ApocTreeListener, 
                              aListenerId - 1) ;
    if (listener->mTreeName == NULL) 
    {
        gconf_log(GCL_WARNING, 
                  _("%s Trying to remove an inexistent listener."), MODULE) ;
        return ;
    }
    holder = g_hash_table_lookup(aTreeAccess->mTrees, listener->mTreeName) ;
    if (holder == NULL || holder->mListeners == NULL) 
    {
        gconf_log(GCL_WARNING, 
                  _("%s Notification has no associated tree."), MODULE) ;
        return ;
    }
    for (i = 0 ; i < holder->mListeners->len ; ++ i)
    {
        if (g_array_index(holder->mListeners, guint, i) == aListenerId)
        {
            g_array_remove_index(holder->mListeners, i) ;
            break ;
        }
    }
    listener->mTreeName = NULL ;
    listener->mCallback = NULL ;
    listener->mUserData = NULL ;
}
                                    

