/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either
 * the GNU General Public License Version 2 only ("GPL") or
 * the Common Development and Distribution License("CDDL")
 * (collectively, the "License"). You may not use this file
 * except in compliance with the License. You can obtain a copy
 * of the License at www.sun.com/CDDL or at COPYRIGHT. See the
 * License for the specific language governing permissions and
 * limitations under the License. When distributing the software,
 * include this License Header Notice in each file and include
 * the License file at /legal/license.txt. If applicable, add the
 * following below the License Header, with the fields enclosed
 * by brackets [] replaced by your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by
 * only the CDDL or only the GPL Version 2, indicate your
 * decision by adding "[Contributor] elects to include this
 * software in this distribution under the [CDDL or GPL
 * Version 2] license." If you don't indicate a single choice
 * of license, a recipient has the option to distribute your
 * version of this file under either the CDDL, the GPL Version
 * 2 or to extend the choice of license to its licensees as
 * provided above. However, if you add GPL Version 2 code and
 * therefore, elected the GPL Version 2 license, then the
 * option applies only if the new code is made subject to such
 * option by the copyright holder.
 */


#include "apoc-parser.h"
#include "apoc-tree-builder.h"
#include "common-utils.h"

#include <libxml/parser.h>
#include <string.h>

static const gchar *MODULE = "[ApocParser]" ;

/** Parsing stack definition */
typedef struct privApocParserStack
{
    gpointer mContent ;                     /** Node or ParsedValue */
    gboolean mIsNode ;                      /** Indicates which it is */
    struct privApocParserStack *mPrevious ; /** Previous element on the stack */
} ApocParserStack ;

static ApocParserStack *apocParserStackPush(ApocParserStack *aStack,
                                            gpointer aContent,
                                            gboolean aIsNode) 
{
    ApocParserStack *retCode = NULL ;

    retCode = g_new0(ApocParserStack, 1) ;
    retCode->mContent = aContent ;
    retCode->mIsNode = aIsNode ;
    retCode->mPrevious = aStack ;
    return retCode ;
}

static ApocParserStack *apocParserStackPop(ApocParserStack *aStack,
                                           gpointer *aContent,
                                           gboolean *aIsNode)
{
    ApocParserStack *retCode = NULL ;
    
    g_return_val_if_fail(aStack != NULL, retCode) ;
    if (aContent != NULL) { *aContent = aStack->mContent ; }
    if (aIsNode != NULL) { *aIsNode = aStack->mIsNode ; }
    retCode = aStack->mPrevious ; 
    g_free(aStack) ;
    return retCode ;
}

static void apocParserStackPeek(ApocParserStack *aStack, 
                                gpointer *aContent,
                                gboolean *aIsNode)
{
    g_return_if_fail(aStack != NULL) ;
    if (aContent != NULL) { *aContent = aStack->mContent ; }
    if (aIsNode != NULL) { *aIsNode = aStack->mIsNode ; }
}

/** Parser context definition. */
struct privApocParserContext
{
    const gchar *mComponent ;   /** Name of component being parsed */
    ApocNode **mRoot ;          /** Root of the tree filled on return */
    ApocParserStack *mStack ;   /** Parsing stack */
    GError **mError ;           /** Last error code */
} ;

/**
  Constructor.
  */
ApocParserContext *createApocParserContext(const gchar *aComponent,
                                           ApocNode **aRoot,
                                           GError **aError)
{
    ApocParserContext *retCode = NULL ;

    g_return_val_if_fail(aComponent != NULL && aRoot != NULL, retCode) ;
    retCode = g_new0(ApocParserContext, 1) ;
    retCode->mComponent = aComponent ;
    retCode->mRoot = aRoot ;
    retCode->mError = aError ;
    return retCode ;
}

/**
  Destructor.
  */
void destroyApocParserContext(ApocParserContext *aContext)
{
    if (aContext != NULL) 
    {
        while (aContext->mStack != NULL)
        {
            aContext->mStack = apocParserStackPop(aContext->mStack, 
                                                  NULL, NULL) ;
        }
        g_free(aContext) ;
    }
}

/** 
  Callbacks of the parsing handler structure. 
  */

/**
  Resolve entity.
  */
static xmlParserInputPtr apocParserResolveEntity(void *aContext, 
                                                 const xmlChar *aPublicId,
                                                 const xmlChar *aSystemId)
{
    return NULL ;
}

/**
  Internal subset declaration.
  */
static void apocParserInternalSubset(void *aContext, const xmlChar *aName, 
                                     const xmlChar *aExternalId,
                                     const xmlChar *aSystemId)
{
}

/**
  External subset declaration.
  */
static void apocParserExternalSubset(void *aContext, const xmlChar *aName, 
                                     const xmlChar *aExternalId,
                                     const xmlChar *aSystemId)
{
}

/**
  Get entity by name.
  */
static xmlEntityPtr apocParserGetEntity(void *aContext, const xmlChar *aName) 
{
    return xmlGetPredefinedEntity(aName) ;
}

/**
  Get parameter entity by name.
  */
static xmlEntityPtr apocParserGetParameterEntity(void *aContext, 
                                                 const xmlChar *aName)
{
    return NULL ;
}

/**
  Entity declaration.
  */
static void apocParserEntityDecl(void *aContext, 
                                 const xmlChar *aName,
                                 int aType,
                                 const xmlChar *aPublicId, 
                                 const xmlChar *aSystemId,
                                 xmlChar *aContent)
{
}

/**
  Notation declaration.
  */
static void apocParserNotationDecl(void *aContext, 
                                   const xmlChar *aName, 
                                   const xmlChar *aPublicId, 
                                   const xmlChar *aSystemId)
{
}

/**
  Attribute declaration.
  */
static void apocParserAttributeDecl(void *aContext, const xmlChar *aElement, 
                                    const xmlChar *aFullName, 
                                    int aType, int aDefault,
                                    const xmlChar *aDefaultValue, 
                                    xmlEnumerationPtr aTree)
{
}

/**
  Element declaration.
  */
static void apocParserElementDecl(void *aContext, const xmlChar *aName, 
                                  int aType, xmlElementContentPtr aContent)
{
}

/**
  Unparsed entity declaration.
  */
static void apocParserUnparsedEntityDecl(void *aContext, 
                                         const xmlChar *aName,
                                         const xmlChar *aPublicId, 
                                         const xmlChar *aSystemId,
                                         const xmlChar *aNotationName)
{
}

/**
  Receive document locator.
  */
static void apocParserSetDocumentLocator(void *aContext, 
                                         xmlSAXLocatorPtr aLocator)
{
}

/** 
  Beginning of the document.
  */
static void apocParserStartDocument(void *aContext)
{
}

/**
  End of the document.
  */
static void apocParserEndDocument(void *aContext) 
{
}

/** 
  Parsed value definition.
  Represents a value while being parsed, i.e the elements 
  accessible between the <value> and the </value> tags.
  */
typedef struct
{
    gchar *mLocale ;
    gchar *mSeparator ;
    GString *mValue ;
} ApocParsedValue ;

/** Constructor of ApocParsedValue. */
static ApocParsedValue *createApocParsedValue(void)
{
    return g_new0(ApocParsedValue, 1) ;
}

/**
  Destructor of ApocParsedValue.
  */
static void destroyApocParsedValue(ApocParsedValue *aValue)
{
    if (aValue != NULL)
    {
        g_free(aValue->mLocale) ;
        g_free(aValue->mSeparator) ;
        if (aValue->mValue != NULL) { g_string_free(aValue->mValue, TRUE) ; }
        g_free(aValue) ;
    }
}

/** Finds the value of an attribute in an array. */
static const gchar *findAttribute(const xmlChar **aAttributes, 
                                  const gchar *aLookedFor)
{
    const gchar **currentAttribute = (const gchar **) aAttributes ; 

    if (currentAttribute == NULL) { return NULL ; }
    while (*currentAttribute != NULL)
    {
        if (strcmp(currentAttribute [0], aLookedFor) == 0)
        {
            return currentAttribute [1] ;
        }
        currentAttribute += 2 ;
    }
    return NULL ;
}

/** Checks if a tag corresponds to a value. */
static gboolean isValue(const xmlChar *aTag)
{
    static const gchar *kValue = "value" ;

    return strcmp((const gchar *) aTag, kValue) == 0 ;
}

/** Identifies the value type of a property from its attribute. */
static ApocValueType findValueType(const gchar *aValueType)
{
    /* Must be aligned on the possible values of ApocValueType. */
    static const gchar *kPossibleTypes [] =
    {
        NULL,               "xs:boolean", 
        "xs:short",         "xs:int", 
        "xs:long",          "xs:double", 
        "xs:string",        "xs:hexBinary", 
        NULL,               NULL, 
        NULL,               NULL, 
        NULL,               NULL, 
        NULL,               NULL,
        NULL,               "oor:boolean-list", 
        "oor:short-list",   "oor:int-list",
        "oor:long-list",    "oor:double-list", 
        "oor:string-list",  "oor:hexBinary-list"
    } ;
    static const gint kNumberTypes = 
                        sizeof(kPossibleTypes) / sizeof(kPossibleTypes [0]) ;
    gint type = 0 ;
    
    if (aValueType == NULL || *aValueType == 0) { return valueType_Unknown ; }
    for (type = 0 ; type < kNumberTypes ; ++ type)
    {
        if (kPossibleTypes [type] != NULL &&
                strcmp(kPossibleTypes [type], aValueType) == 0)
        {
            return type ;
        }
    }
    return valueType_Unknown ;
}

/** Checks if a tag corresponds to a property node. */
static gboolean isPropertyNode(const xmlChar *aTag)
{
    static const gchar *kProperty = "prop" ;

    return strcmp((const gchar *) aTag, kProperty) == 0 ;
}

/** Checks if a tag corresponds to a structural node */
static gboolean isStructuralNode(const xmlChar *aTag)
{
    static const gchar *kStructuralTags [] = 
    {
        "node", "oor:node", 
        "oor:component-data", 
        "group", "oor:group", 
        "set", "oor:set", 
        NULL
    } ;
    const gchar **currentTag = kStructuralTags ;

    while (*currentTag != NULL)
    {
        if (strcmp(*currentTag, (const gchar *) aTag) == 0) { return TRUE ; }
        ++ currentTag ;
    }
    return FALSE ;
}

/**
  Computes whether a node is finalised depending on its attributes 
  and its parent's status.
  */
static gboolean isFinalised(ApocStructuralNode *aParent, 
                            const xmlChar **aAttributes)
{
    static const gchar *kFinalised = "oor:finalized" ;
    gboolean isParentFinalised = FALSE ;
    const gchar *finalisedAttribute = findAttribute(aAttributes, kFinalised) ;

    if (aParent != NULL) 
    {
        isParentFinalised = apocNodeIsFinalised((ApocNode *) aParent) ; 
    }
    if (finalisedAttribute != NULL)
    {
        return strcmp(finalisedAttribute, "true") == 0 ;
    }
    return isParentFinalised ;
}

/** XML attributes representing the node characteristics. */
static const gchar *kNodeName = "oor:name" ;
static const gchar *kValueType = "oor:type" ;

/**
  Generic helper to create a new node corresponding to what's being parsed.
  */
static ApocNode *apocParserCreateNode(ApocParserContext *aContext,
                                      const gchar *aName,
                                      ApocStructuralNode *aParent,
                                      const xmlChar **aAttributes,
                                      gboolean aIsStructural)
{
    if (aIsStructural)
    {   /* Structural node to be built. */
        return (ApocNode *) createApocStructuralNode(aName, 
                                            isFinalised(aParent, aAttributes), 
                                            aContext->mError) ;
    }
    else 
    {
        ApocValueType valueType = valueType_Unknown ;

        valueType = findValueType(findAttribute(aAttributes, kValueType)) ;
        return (ApocNode *) createApocPropertyNode(aName, 
                                            isFinalised(aParent, aAttributes),
                                            valueType,
                                            aContext->mError) ;
    }
}

/**
  Modify existing node. 
  If a node is found in the tree, it is modified according to the 
  attributes of the node being parsed. Otherwise, a new node is
  just added.
  */
static ApocNode *apocParserModifyNode(ApocParserContext *aContext,
                                      const gchar *aName,
                                      ApocStructuralNode *aParent,
                                      const ApocNode *aInitialNode,
                                      const xmlChar **aAttributes,
                                      gboolean aIsStructural)
{
    if (aInitialNode == NULL) 
    {   /* No existing node by the same name, so just put a new one. */
        return apocParserCreateNode(aContext, aName, aParent,
                                    aAttributes, aIsStructural) ;
    }
    /* TODO Handle removal of finalised status. */
    if (apocNodeIsFinalised(aInitialNode))
    {   /* If the original node is finalised, it cannot be modified, 
           however if it's a structural node, one of its children may
           be un-finalised, so we have to keep parsing. Property nodes
           cannot have children so to prevent modifying their value we
           return NULL. */
        return aIsStructural ? (ApocNode *) aInitialNode : NULL ;
    }
    else
    {   /* Check if the new node is finalised. */
        if (isFinalised(NULL, aAttributes))
        {
            apocNodeSetFinalised((ApocNode *) aInitialNode, TRUE) ;
        }
    }
    return (ApocNode *) aInitialNode ;
}

/**
  Removes a node.
  If a node by the proper name is found in the tree, it is deleted,
  otherwise we just don't do anything.
  */
static ApocNode *apocParserRemoveNode(ApocParserContext *aContext,
                                      const gchar *aName,
                                      ApocStructuralNode *aParent,
                                      const ApocNode *aInitialNode,
                                      const xmlChar **aAttributes, 
                                      gboolean isStructural)
{
    if (aParent != NULL && aInitialNode != NULL) 
    {
        if (apocNodeIsFinalised(aInitialNode))
        {   /* If the original node is finalised, it cannot be removed,
               and we return NULL to have the whole subtree being
               parsed ignored by the merging process. */
            return NULL ;
        }
        apocNodeDestroyChild(aParent, aInitialNode) ;
    }
    /* And anyway we don't care about anything that might be present
       under the parsed node since it's been removed or ignored. */
    return NULL ;
}

/**
  Add or replace node.
  This puts a newly created node in the tree. If a node by the same
  name already exists, it is discarded before proceeding.
  */
static ApocNode *apocParserReplaceNode(ApocParserContext *aContext, 
                                       const gchar *aName,
                                       ApocStructuralNode *aParent,
                                       const ApocNode *aInitialNode,
                                       const xmlChar **aAttributes,
                                       gboolean aIsStructural)
{
    if (aParent != NULL && aInitialNode != NULL) 
    { 
        if (apocNodeIsFinalised(aInitialNode))
        {   /* If the original node is finalised, it cannot be replaced,
               and we return NULL to have the whole subtree being
               parsed ignored by the merging process. */
            return NULL ;
        }
        apocNodeDestroyChild(aParent, aInitialNode) ;
    }
    return apocParserCreateNode(aContext, aName, aParent, 
                                aAttributes, aIsStructural) ;
}

/**
  Possible operations on a node and its parsing method.
  */
typedef enum
{
    parserOperation_Modify = 0, /* Modify the node attributes or contents. */
    parserOperation_Remove,     /* Remove the node and its contents. */
    parserOperation_AddReplace  /* Replace the node and its contents. */
} ApocParserOperation ;

static ApocParserOperation findOperationType(const gchar *aOperation) 
{
    ApocParserOperation retCode = parserOperation_Modify ;
    static const gchar *kOperationModify = "modify" ;
    static const gchar *kOperationRemove = "remove" ;
    static const gchar *kOperationReplace = "replace" ;

    if (aOperation != NULL) 
    {
        if (strcmp(aOperation, kOperationReplace) == 0) 
        {
            retCode = parserOperation_AddReplace ;
        }
        else if (strcmp(aOperation, kOperationRemove) == 0)
        {
            retCode = parserOperation_Remove ;
        }
        else if (strcmp(aOperation, kOperationModify) != 0) 
        {
            gconf_log(GCL_WARNING, 
                      _("%s Malformed data, operation %s unknown."),
                      MODULE, aOperation) ;
        }
    }
    /* Anyway, we always default to modify. */
    return retCode ;
}

/**
  Merge node.
  Takes the current state of the tree and performs the operation
  prescribed in the currently parsed node on it. 
  The resulting node (either a new node or an existing one from the
  tree being modified or null if the node has actually been removed)
  is returned.
  */
static ApocNode *apocParserMergeNode(ApocParserContext *aContext, 
                                     const xmlChar **aAttributes,
                                     gboolean aIsStructural)
{
    ApocParserOperation operation = parserOperation_Modify ;
    const gchar *name = NULL ;
    ApocStructuralNode *parent = NULL ;
    const ApocNode *initialNode = NULL ;
    static const gchar *kOperation = "oor:op" ;

    operation = findOperationType(findAttribute(aAttributes, kOperation)) ;
    name = findAttribute(aAttributes, kNodeName) ;
    g_return_val_if_fail(name != NULL, NULL) ;
    if (aContext->mStack != NULL) 
    {
        gboolean isStructural = FALSE ;

        apocParserStackPeek(aContext->mStack, (gpointer *) &parent, 
                            &isStructural) ;
        /* TODO Some validation with isStructural. */
        if (parent != NULL)
        {
            initialNode = apocNodeFindChild(parent, name) ;
        }
    }
    else
    {
        initialNode = *(aContext->mRoot) ;
    }
    switch (operation)
    {
        case parserOperation_Modify:
            return apocParserModifyNode(aContext, name, parent, initialNode,
                                        aAttributes, aIsStructural) ;
        case parserOperation_Remove:
            return apocParserRemoveNode(aContext, name, parent, initialNode,
                                        aAttributes, aIsStructural) ;
        case parserOperation_AddReplace:
            return apocParserReplaceNode(aContext, name, parent, initialNode,
                                         aAttributes, aIsStructural) ;
        default:
            return NULL ;
    }
}

/**
  Start of an element.
  Creates an appropriate configuration node and puts it on the stack.
  If parsing the root node, checks the component name for consistency.
  */
static void apocParserStartElement(void *aContext, const xmlChar *aName,
                                   const xmlChar **aAttributes)
{
    ApocParserContext *context = aContext ;
    ApocNode *newNode = NULL ;
    ApocParsedValue *newValue = NULL ;

    if (context->mStack == NULL)
    {   /* Root node. */
        /* TODO Check component name. */
    }
    if (isValue(aName))
    {
        static const gchar *kSeparator = "oor:separator" ;
        static const gchar *kLanguage = "xml:lang" ;

        newValue = createApocParsedValue() ;
        newValue->mLocale = g_strdup(findAttribute(aAttributes, kLanguage)) ;
        newValue->mSeparator = g_strdup(findAttribute(aAttributes,
                                                      kSeparator)) ;
    }
    else
    {
        gboolean isStructural = isStructuralNode(aName) ;

        if (isStructural || isPropertyNode(aName))
        {
            newNode = apocParserMergeNode(context, aAttributes, isStructural) ;
        }
    }
    if (newNode != NULL)
    {
        context->mStack = apocParserStackPush(context->mStack, 
                                              newNode, TRUE) ;
    }
    else if (newValue != NULL)
    {
        context->mStack = apocParserStackPush(context->mStack, 
                                              newValue, FALSE) ;
    }
    else
    {   /* Not good. 
           For the moment, stack something to not disturb the process. */
        gconf_log(GCL_WARNING, 
                  _("%s Malformed data, couldn't find type of %s."),
                  MODULE, aName) ;
        context->mStack = apocParserStackPush(context->mStack, NULL, FALSE) ;
    }
}

/**
  End of an element.
  Pops the stack and connects the nodes.
  */
static void apocParserEndElement(void *aContext, const xmlChar *aName)
{
    ApocParserContext *context = aContext ;
    ApocNode *currentNode = NULL ;
    ApocParsedValue *currentValue = NULL ;
    gpointer stackedObject = NULL ;
    gboolean isNode = FALSE ;
    
    context->mStack = apocParserStackPop(context->mStack, 
                                         &stackedObject, &isNode) ;
    if (stackedObject == NULL)
    {   /* Something we didn't like when entering, ignore */
        return ;
    }
    if (isNode)
    {
        if (context->mStack != NULL)
        {   /* We didn't pop the root. */
            ApocStructuralNode *parent = NULL ;

            currentNode = stackedObject ;
            apocParserStackPeek(context->mStack, (gpointer *) &parent, NULL) ;
            apocNodeAddChild(parent, currentNode, context->mError) ;
        }
        else 
        {   /* We reached the root node, let's fill it in the context. */
            *(context->mRoot) = stackedObject ;
        }
    }
    else
    {
        ApocPropertyNode *property = NULL ;
        
        currentValue = stackedObject ;
        apocParserStackPeek(context->mStack, (gpointer *) &property, NULL) ;
        if (currentValue->mValue != NULL)
        {
            apocNodeAddValue(property, 
                             currentValue->mLocale, 
                             currentValue->mSeparator,
                             currentValue->mValue->str,
                             context->mError) ;
        }
        destroyApocParsedValue(currentValue) ;
    }
}

/**
  Handle reference.
  */
static void apocParserReference(void *aContext, const xmlChar *aName)
{
}

/**
  Characters.
  Adds the characters to the value in progress, if any.
  */
static void apocParserCharacters(void *aContext, 
                                 const xmlChar *aCharacters, int aLength)
{
    ApocParserContext *context = aContext ;

    if (context->mStack != NULL)
    {
        ApocParsedValue *value = NULL ;
        gboolean isNode = FALSE ;

        apocParserStackPeek(context->mStack, (gpointer *) &value, &isNode) ;
        if (!isNode && value != NULL)
        {
            if (value->mValue == NULL)
            {
                value->mValue = g_string_new_len((const gchar *) aCharacters, 
                                                 aLength) ;
            }
            else 
            {
                g_string_append_len(value->mValue, 
                                    (const gchar *) aCharacters, 
                                    aLength) ;
            }
        }
    }
}

/**
  Handle ignorable whitespace.
  */
static void apocParserIgnorableWhitespace(void *aContext, 
                                          const xmlChar *aCharacters,
                                          int aLength)
{
}

/**
  Handle processing instruction.
  */
static void apocParserProcessingInstruction(void *aContext, 
                                            const xmlChar *aTarget,
                                            const xmlChar *aData)
{
}

/**
  Comment.
  */
static void apocParserComment(void *aContext, const xmlChar *aValue)
{
}

/**
  CDATA block.
  */
static void apocParserCdataBlock(void *aContext, const xmlChar *aValue, 
                                 int aLength)
{
}

/**
  Warning.
  */
static void apocParserWarning(void *aContext, 
                              const char *aMessage, ...)
{
}

/**
  Error.
  */
static void apocParserError(void *aContext,
                            const char *aMessage, ...) 
{
}

/**
  Fatal error.
  */
static void apocParserFatalError(void *aContext,
                                 const char *aMessage, ...)
{
}

/**
  Is the document standalone?
  */
static int apocParserIsStandalone(void *aContext) { return 1 ; }

/**
  Does the document have an internal subset?
  */
static int apocParserHasInternalSubset(void *aContext) { return 0 ; }

/**
  Does the document have an external subset?
  */
static int apocParserHasExternalSubset(void *aContext) { return 0 ; }

/**
  Parsing of an XML blob.
  */
void apocParserParseBlob(ApocParserContext *aContext, 
                         const guchar *aBlob, gsize aSize)
{
    gint retCode = 0 ;
    static xmlSAXHandler kApocParserHandler = 
    {
        apocParserInternalSubset,
        apocParserIsStandalone,
        apocParserHasInternalSubset,
        apocParserHasExternalSubset,
        apocParserResolveEntity,
        apocParserGetEntity,
        apocParserEntityDecl,
        apocParserNotationDecl,
        apocParserAttributeDecl,
        apocParserElementDecl,
        apocParserUnparsedEntityDecl,
        apocParserSetDocumentLocator,
        apocParserStartDocument,
        apocParserEndDocument,
        apocParserStartElement,
        apocParserEndElement,
        apocParserReference,
        apocParserCharacters,
        apocParserIgnorableWhitespace,
        apocParserProcessingInstruction,
        apocParserComment,
        apocParserWarning,
        apocParserError,
        apocParserFatalError,
        apocParserGetParameterEntity,
        apocParserCdataBlock,
        apocParserExternalSubset,
        0
    } ;
    g_return_if_fail(aContext != NULL && aBlob != NULL) ;
    retCode = xmlSAXUserParseMemory(&kApocParserHandler, aContext, 
                                    (const gchar *) aBlob, aSize) ;
    if (retCode != 0)
    {
        gconf_log(GCL_ERR, 
                  _("%s Parsing of blob failed (%d)."), 
                  MODULE, retCode) ;
    }
}

