/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either
 * the GNU General Public License Version 2 only ("GPL") or
 * the Common Development and Distribution License("CDDL")
 * (collectively, the "License"). You may not use this file
 * except in compliance with the License. You can obtain a copy
 * of the License at www.sun.com/CDDL or at COPYRIGHT. See the
 * License for the specific language governing permissions and
 * limitations under the License. When distributing the software,
 * include this License Header Notice in each file and include
 * the License file at /legal/license.txt. If applicable, add the
 * following below the License Header, with the fields enclosed
 * by brackets [] replaced by your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by
 * only the CDDL or only the GPL Version 2, indicate your
 * decision by adding "[Contributor] elects to include this
 * software in this distribution under the [CDDL or GPL
 * Version 2] license." If you don't indicate a single choice
 * of license, a recipient has the option to distribute your
 * version of this file under either the CDDL, the GPL Version
 * 2 or to extend the choice of license to its licensees as
 * provided above. However, if you add GPL Version 2 code and
 * therefore, elected the GPL Version 2 license, then the
 * option applies only if the new code is made subject to such
 * option by the copyright holder.
 */


#include "apoc-tree.h"
#include "apoc-tree-builder.h"
#include "apoc-parser.h"
#include "apoc-papi.h"
#include "common-utils.h"

#include <string.h>

static const gchar *MODULE =  "[ApocTree]" ;

/** Node path definition */
struct privApocNodePath
{
    /** Component name */
    gchar *mComponent ;
    /** Separated path elements. They're all part of one big string. */
    gchar **mPathElements ;
} ;

/** Accessor to the component part of the node path. */
const gchar *apocNodePathGetComponent(const ApocNodePath *aPath)
{
    g_return_val_if_fail(aPath != NULL, NULL) ;
    return aPath->mComponent ;
}

/**
  Adds to the component string the subdirectory being pointed to 
  and moves the pointers to the next one.
  */
static void addComponentPart(const gchar **aKey, const gchar **aCurrentSlash,
                             GString *aComponent)
{
    if (**aKey == 0) { return ; }
    g_string_append_c(aComponent, '.') ;
    if (*aCurrentSlash == NULL)
    {
        g_string_append(aComponent, *aKey) ;
        while (**aKey != 0) { ++ *aKey ; }
    }
    else 
    {
        g_string_append_len(aComponent, *aKey, *aCurrentSlash - *aKey) ;
        *aKey = *aCurrentSlash + 1 ;
        *aCurrentSlash = strchr(*aKey, '/') ;
    }
}

/**
  Identify whether the subdirectory we're pointing at is the
  schema special directory, if so add it to the component
  and move to the next subdirectory.
  */
static void handleSchema(const gchar **aKey, const gchar **aCurrentSlash,
                         GString *aComponent)
{
    static const gchar *kSchemaPart = "schemas" ;

    if ((*aCurrentSlash == NULL && strcmp(*aKey, kSchemaPart) == 0) ||
            (*aCurrentSlash != NULL &&
             strncmp(*aKey, kSchemaPart, *aCurrentSlash - *aKey) == 0)) 
    {
        addComponentPart(aKey, aCurrentSlash, aComponent) ;
    }
}

/** Prefix for all GConf mapped components. */
#define ORG_GNOME_PREFIX    "org.gnome"

const gchar *apocNodePathGetCommonPrefix(void)
{
    return ORG_GNOME_PREFIX "." ;
}

/** Identifies the Out Of Convention (OOC) keys in the component structure. */
#define OOC_PART    ".ooc"

/** 
  Aggregation of the prefix and OOC indicator, which represents the
  "root" ApocNodePath (i.e what '/' gets mapped to).
  */
static const gchar *kRootPath = ORG_GNOME_PREFIX OOC_PART ;

/** List of special directories in the mapping algorithm. */
static const gchar *kSpecialDirectories [] = 
                                { "apps", "desktop", "system", "extra", NULL } ;
/**
  Identify whether the subdirectory we're pointing at is a special 
  subdirectory, in which case we add it to the component and move to
  the next subdirectory, otherwise we insert the OOC indicator and
  keep the subdirectory for the next step.
  */
static void handleSpecial(const gchar **aKey, const gchar **aCurrentSlash,
                          GString *aComponent)
{
    const gchar **currentSpecial = kSpecialDirectories ;

    while (*currentSpecial != NULL)
    {
        if ((*aCurrentSlash == NULL && strcmp(*aKey, *currentSpecial) == 0) ||
                (*aCurrentSlash != NULL &&
                 strncmp(*aKey, *currentSpecial, *aCurrentSlash - *aKey) == 0))
        {   /* The subdirectory is one of the specials, 
               we add it and exit. */
            addComponentPart(aKey, aCurrentSlash, aComponent) ;
            return ;
        }
        ++ currentSpecial ;
    }
    /* If we're here, the current subdirectory is not a special one,
       so we put the OOC indicator in the component but don't move
       the pointers. */
    g_string_append(aComponent, OOC_PART) ;
}

/**
  Utility applying the encoding rules on a directory key
  to build a component name and provide the start of the 
  path elements.
  */
static void getComponentAndPathStart(const gchar *aKey, 
                                     gchar **aComponent, 
                                     const gchar **aStartPath)
{
    const gchar *slash = NULL ;
    GString *component = NULL ;

    component = g_string_new(ORG_GNOME_PREFIX) ;
    if (*aKey == 0) 
    {   /* Empty string, easy case. */
        g_string_append(component, OOC_PART) ;
        *aComponent = g_string_free(component, FALSE) ;
        *aStartPath = aKey ;
        return ;
    }
    /* Ignore the initial slash */
    ++ aKey ;
    slash = strchr(aKey, '/') ;
    /* First, see if we have a schema subdir. */
    handleSchema(&aKey, &slash, component) ;
    /* Then see if there's a special subdir. */
    handleSpecial(&aKey, &slash, component) ;
    /* And if there's some more, take one subdir. */
    addComponentPart(&aKey, &slash, component) ;
    /* What's left is the path or the end of the string. */
    *aStartPath = aKey ;
    *aComponent = g_string_free(component, FALSE) ;
}

/**
  Splits a string containing slashes by replacing the slashes
  with zeros and returning an array of pointers to the various
  substrings in the initial one. The parameter is untouched as
  it is duplicated. Callers hence need to free both the array 
  and the first element thereof which points to the overall buffer.
  */
static gchar **splitPath(const gchar *aPath) 
{
    gchar *path = NULL ;
    guint nbTokens = 0 ;
    gchar *iterator = NULL ;
    gchar *endPath = NULL ;
    gchar **retCode = NULL ;

    if (aPath == NULL || *aPath == 0) { return NULL ; }
    path = g_strdup(aPath) ;
    endPath = path + strlen(path) ;
    nbTokens = 1 ; /* At least the path itself */
    iterator = strchr(path, '/') ;
    while (iterator != NULL) 
    {
        *iterator = 0 ; 
        ++ nbTokens ;
        iterator = strchr(iterator + 1, '/') ;
    }
    retCode = g_malloc0((nbTokens + 1) * sizeof (gchar *)) ;
    nbTokens = 0 ;
    iterator = path ;
    while (iterator < endPath)
    {
        retCode [nbTokens ++] = iterator ;
        while (*iterator != 0) { ++ iterator ; }
        ++ iterator ;
    } 
    retCode [nbTokens] = NULL ;
    return retCode ;
}

/**
  Constructor from GConf key.
  If aIsDirectory is set, the last part of the key is considered
  as a subdirectory (and possibly put into the component),
  otherwise the last part is deemed a setting (and necessarily put
  into the path).
  */
ApocNodePath *gconfKeyToApocNodePath(const gchar *aKey, gboolean aIsDirectory,
                                     GError **aError)
{
    ApocNodePath *retCode = NULL ;
    gchar *lastSlash = NULL ;
    const gchar *startPath = NULL ;
    gchar *key = NULL ;

    g_return_val_if_fail(aKey != NULL && *aKey == '/', retCode) ;
    key = g_strdup(aKey) ;
    retCode = g_new0(ApocNodePath, 1) ;
    if (!aIsDirectory)
    {   /* The last part is excluded of the component search. */
        lastSlash = strrchr(key, '/') ;
        *lastSlash = 0 ;
    }
    else
    {   /* Remove possible trailing slash. */
        lastSlash = (gchar *) (key + (strlen(key) - 1)) ;
        if (*lastSlash == '/') { *lastSlash = 0 ; }
    }
    getComponentAndPathStart(key, &retCode->mComponent, &startPath) ;
    /* Restore the last part for the settings, so that it's taken
       into account during the path calculation. */
    if (!aIsDirectory) 
    {
        *lastSlash = '/' ;
        if (startPath == lastSlash)
        {   /* Special case where the key is directly located 
               under the component. We need to skip the newly
               restored slash to present the path (in this case
               just the key) in the way expected by splitPath,
               i.e at the relative subpath beginning. */
            ++ startPath ;
        }
    }
    retCode->mPathElements = splitPath(startPath) ;
    g_free(key) ;
    return retCode ;
}

/**
  Returns the root of a node path.
  */
ApocNodePath *apocNodePathGetRoot(const ApocNodePath *aPath) 
{
    ApocNodePath *retCode = NULL ;

    g_return_val_if_fail(aPath != NULL && aPath->mComponent != NULL, retCode) ;
    retCode = g_new0(ApocNodePath, 1) ;
    retCode->mComponent = g_strdup(aPath->mComponent) ;
    return retCode ;
}

/**
  Indicates whether a node path is special, i.e could have its data
  held in multiple trees. This amounts to the node path only containing
  a component.
  TODO We could make more complicated checks to trap node paths which 
  only have a component but which couldn't have subcomponents.
  */
gboolean apocNodePathIsSpecial(const ApocNodePath *aPath)
{
    return aPath != NULL && aPath->mPathElements == NULL ;
}

/**
  Checks whether a tree contains (or could contain) data for a given path.
  This amounts to checking if the path is a substring of the tree name, 
  with the caveat that the root path contains everything within the gnome
  namespace.
  */
gboolean apocNodePathIsTree(const gchar *aPath, const gchar *aTree)
{
    const gchar *filter = NULL ;
    guint filterLength = 0 ;
    
    g_return_val_if_fail(aPath != NULL && aTree != NULL, FALSE) ;
    if (strcmp(aPath, kRootPath) == 0) 
    {
        filter = ORG_GNOME_PREFIX ;
    }
    else
    {
        filter = aPath ; 
    }
    filterLength = strlen(filter) ;
    return strncmp(aTree, filter, filterLength) == 0 &&
           aTree [filterLength] == '.' ;
}

/**
  Returns the common part of two paths.
  Checks that both paths are in the same component and compares
  the path elements until a discrepancy appears. If one path
  is the common part, it gets returned otherwise a new path is
  created with the common bits in it.
  */
ApocNodePath *apocNodePathGetCommonPath(const ApocNodePath *aPath1,
                                        const ApocNodePath *aPath2) 
{
    gchar **pathElements1 = NULL ;
    gchar **pathElements2 = NULL ;

    g_return_val_if_fail(aPath1 != NULL && aPath2 != NULL, NULL) ;
    if (strcmp(aPath1->mComponent, aPath2->mComponent) != 0) { return NULL ; }
    pathElements1 = aPath1->mPathElements ;
    pathElements2 = aPath2->mPathElements ;
    if (pathElements1 == NULL) {  return (ApocNodePath *) aPath1 ; }
    if (pathElements2 == NULL) {  return (ApocNodePath *) aPath2 ; }
    while (*pathElements1 != NULL) 
    {
        if (*pathElements2 == NULL) { return (ApocNodePath *) aPath2 ; }
        if (strcmp(*pathElements1, *pathElements2) != 0) 
        {   /* Need to create a common path. */
            int nbTokens = pathElements1 - aPath1->mPathElements ;
            const gchar *sourceString = *(aPath1->mPathElements) ;
            gchar *targetString = NULL ;
            int stringLength = *pathElements1 - sourceString ;
            int i = 0 ;
            ApocNodePath *retCode = g_new0(ApocNodePath, 1) ;

            retCode->mComponent = g_strdup(aPath1->mComponent) ;
            retCode->mPathElements = 
                                g_malloc0((nbTokens + 1) * sizeof(gchar *)) ;
            targetString = g_malloc0(stringLength) ;
            memcpy(targetString, sourceString, stringLength) ;
            for (i = 0 ; i < nbTokens ; ++ i)
            {
                retCode->mPathElements [i] = targetString +
                                             (aPath1->mPathElements [i] - 
                                              sourceString) ;
            }
            retCode->mPathElements [i] = NULL ;
            return retCode ;
        }
        ++ pathElements1 ; 
        ++ pathElements2 ;
    }
    return (ApocNodePath *) aPath1 ;
}

/**
  Lists the sub-components from a node path.
  In the case where the node path doesn't point within a component,
  i.e it has no path, existing components sharing the same prefix
  as the node path's component are mapped subdirectories of the
  node path.
  */
static GSList *apocNodePathListSubComponents(
                                        const ApocNodePath *aPath,
                                        const ApocPapi *aPapi,
                                        const ApocPapiConnection *aConnection,
                                        GSList *aList,
                                        GError **aError)
{
    PAPIStringList *componentList = NULL ; 
    PAPIStringList *iterator = NULL ;
    gint prefixLength = 0 ;
    PAPIStatus status = PAPISuccess ;

    /* If we're disconnected, no data will be there anyway. */
    if (aConnection->mDisconnected) { return aList ; }
    /* If we're within a component, no point looking. */
    if (aPath == NULL || 
            aPath->mComponent == NULL || 
            aPath->mPathElements != NULL) { return aList ; }
    /* First get all components matching the prefix. */
    componentList = aPapi->mListComponentNames(aConnection->mContext,
                                               aPath->mComponent, 
                                               &status) ;
    if (status != PAPISuccess) 
    {
        gconf_log(GCL_WARNING,
                  _("%s Error listing component names: %s"), MODULE, 
                  apocPapiGetError(status)) ;
        return aList ;
    }
    /* Then extract the subcomponents from the list. */
    prefixLength = strlen(aPath->mComponent) ;
    for (iterator = componentList ; 
            iterator != NULL ; iterator = iterator->mNext)
    {
        const gchar *component = iterator->mData ;
        
        if (strncmp(component, aPath->mComponent, prefixLength) == 0 &&
                component [prefixLength] == '.')
        {   /* We got a match, we copy the remainder of the component name */
            aList = g_slist_append(aList, 
                                   g_strdup(component + prefixLength + 1)) ;
        }
    }
    aPapi->mFreeStringList(componentList) ;
    if (strcmp(aPath->mComponent, kRootPath) == 0)
    {   /* In the case of the root path, we add the specials,
           which we assume to always be there (simpler than 
           checking if they actually are). */
        const gchar **currentSpecial = kSpecialDirectories ;

        while (*currentSpecial != NULL) 
        {
            aList = g_slist_append(aList, g_strdup(*currentSpecial)) ;
            ++ currentSpecial ;
        }
    }
    return aList ;
}

/**
  Similar to ListSubComponents but only checks for the existence of
  the path or a sub component thereof in the PAPI.
  */
static gboolean apocNodePathHasSubComponents(
                                        const ApocNodePath *aPath,
                                        const ApocPapi *aPapi,
                                        const ApocPapiConnection *aConnection,
                                        GError **aError)
{
    PAPIStringList *componentList = NULL ;
    PAPIStringList *iterator = NULL ;
    guint prefixLength = 0 ;
    PAPIStatus status = PAPISuccess ;

    g_return_val_if_fail(aPath != NULL && 
                         aPapi != NULL &&
                         aConnection != NULL, FALSE) ;
    if (aPath->mComponent == NULL || aPath->mPathElements != NULL)
    {
        return FALSE ;
    }
    if (strcmp(aPath->mComponent, kRootPath) == 0)
    {   /* We assume the specials are here so the root component
           always has subcomponents. */
        return TRUE ;
    }
    if (aConnection->mDisconnected) { return FALSE ; }
    componentList = aPapi->mListComponentNames(aConnection->mContext,
                                               aPath->mComponent,
                                               &status) ;
    if (status != PAPISuccess) { return FALSE ; }
    prefixLength = strlen(aPath->mComponent) ;
    for (iterator = componentList ;
            iterator != NULL ; iterator = iterator->mNext)
    {
        const gchar *component = iterator->mData ;

        if (strncmp(component, aPath->mComponent, prefixLength) == 0 &&
                (component [prefixLength] == 0 ||
                 component [prefixLength] == '.'))
        {
            return TRUE ;
        }
    }
    return FALSE ;
}

/**
  Returns the node path corresponding to the replacement nodes.
  */
ApocNodePath *apocNodePathGetReplacementPath(void)
{
    static const gchar *kReplacementComponent = "com.sun.jds.UserProfile" ;
    static const gchar *kReplacementPath = "Data" ;
    ApocNodePath *retCode = g_new0(ApocNodePath, 1) ;

    retCode->mComponent = g_strdup(kReplacementComponent) ;
    retCode->mPathElements = splitPath(kReplacementPath) ;
    return retCode ;
}

/**
  Destructor of a node path.
  The path elements are in fact pointers in a big string which 
  is freed in one go.
  */
void destroyApocNodePath(ApocNodePath *aPath)
{
    if (aPath != NULL)
    {
        g_free(aPath->mComponent) ;
        if (aPath->mPathElements != NULL) 
        {
            g_free(*aPath->mPathElements) ;
            g_free(aPath->mPathElements) ;
        }
        g_free(aPath) ;
    }
}

/** Configuration tree definition */
struct privApocTree
{
    gchar *mName ;                          /** Component name */
    ApocNode *mRoot ;                       /** Root of the tree */
    GTime mTimestamp ;                      /** Time of last modification */
    const ApocPapi *mPapi ;                 /** Policy access API */
    const ApocPapiConnection *mConnection ; /** PAPI connection */
} ;

/**
  Generates a node from blobs.
  Uses the layers in the layer list to get the root of the
  parsed and merged configuration tree.
  */
static ApocNode *createApocNodeFromBlobs(const gchar *aComponent,
                                         PAPILayerList *aBlobs,
                                         GError **aError)
{
    ApocNode *retCode = NULL ;
    ApocParserContext *context = NULL ;
                                                                      
    g_return_val_if_fail(aComponent != NULL && aBlobs != NULL, retCode) ;
    context = createApocParserContext(aComponent, &retCode, aError) ;
    do
    {
        apocParserParseBlob(context, aBlobs->mData, aBlobs->mSize) ;
        aBlobs = aBlobs->mNext ;
    } while (aBlobs != NULL) ;
    destroyApocParserContext(context) ;
    return retCode ;
}

/**
  Performs the actual loading of a data tree.
  */
static void apocTreeLoad(ApocTree *aTree, GError **aError)
{
    PAPILayerList *blobs = NULL ;
    PAPIStatus status = PAPISuccess ;
    
    g_return_if_fail(aTree != NULL) ;
    if (aTree->mConnection->mDisconnected) { return ; }
    blobs = aTree->mPapi->mReadComponentLayers(aTree->mConnection->mContext,
                                               aTree->mName, 
                                               &status) ;
    if (status != PAPISuccess && status != PAPINoSuchComponentFailure) 
    {
        gconf_log(GCL_WARNING,
                  _("%s Error reading layers for %s: %s"), MODULE, 
                  aTree->mName, apocPapiGetError(status)) ;
        return ;
    }
    if (blobs == NULL) { return ; }
    aTree->mRoot = createApocNodeFromBlobs(aTree->mName, blobs, aError) ;
    aTree->mPapi->mFreeLayerList(blobs) ;
    if (aTree->mRoot != NULL) 
    {
        apocNodeSetTree(aTree->mRoot, aTree) ;
    }
}

/**
  Constructor from node path.
  Loads the contents of the tree using the PAPI.
  */
ApocTree *createApocTree(const ApocNodePath *aPath, 
                         const ApocPapi *aPapi,
                         const ApocPapiConnection *aConnection,
                         GError **aError)
{
    ApocTree *retCode = NULL ;
    
    g_return_val_if_fail(aPath != NULL, retCode) ;
    retCode = g_new0(ApocTree, 1) ;
    retCode->mName = g_strdup(aPath->mComponent) ;
    retCode->mPapi = aPapi ;
    retCode->mConnection = aConnection ;
    apocTreeLoad(retCode, aError) ;
    return retCode ;
}

/**
  Shallow copy constructor of a tree.
  The contents of the tree are not copied but reloaded.
  */
ApocTree *cloneApocTree(const ApocTree *aTree, GError **aError)
{
    ApocTree *retCode = NULL ;

    g_return_val_if_fail(aTree != NULL, retCode) ;
    retCode = g_new0(ApocTree, 1) ;
    retCode->mName = g_strdup(aTree->mName) ;
    retCode->mTimestamp = aTree->mTimestamp ;
    retCode->mPapi = aTree->mPapi ;
    retCode->mConnection = aTree->mConnection ;
    apocTreeLoad(retCode, aError) ;
    return retCode ;
}

/**
  Returns the name (i.e the component) of the tree.
  */
const gchar *apocTreeGetName(const ApocTree *aTree)
{
    g_return_val_if_fail(aTree != NULL, NULL) ;
    return aTree->mName ;
}

/**
  Utility to transform a dot separated sequence representing
  the remainder of a component name into a GConf path by 
  replacing the dots with slashes.
  */
static GString *dotToSlashes(const gchar *aString) 
{
    GString *retCode = NULL ;
    gchar *iterator = NULL ;
    
    if (aString == NULL) { aString = "" ; } 
    retCode = g_string_new(aString) ;
    iterator = retCode->str ;
    while (*iterator != 0)
    {
        if (*iterator == '.') { *iterator = '/' ; }
        ++ iterator ;
    }
    return retCode ;
}

/**
  Builds a string containing the name of the tree seen as a 
  GConf path.
  */
GString *apocTreeGetPath(const ApocTree *aTree)
{
    const gchar *name = NULL ;
    guint length = 0 ;
    
    g_return_val_if_fail(aTree != NULL, NULL) ;
    name = aTree->mName ;
    /* The name begins with the common component part */
    length = strlen(ORG_GNOME_PREFIX) ;
    if (strncmp(name, ORG_GNOME_PREFIX, length) == 0)
    {
        name += length ;
        length = strlen(OOC_PART) ;
        if (strncmp(name, OOC_PART, length) == 0)
        {   /* If we have an ooc, we skip it */
            name += length ;
        }
        /* And anyway we end up with something empty or 
           a dot separated sequence where every item
           is a path element. */
        return dotToSlashes(name) ;
    }
    return NULL ;
}

/**
  Returns the modification time of the tree.
  */
GTime apocTreeGetModificationTime(const ApocTree *aTree)
{
    g_return_val_if_fail(aTree != NULL, 0) ;
    return aTree->mTimestamp ;
}

/**
  Returns node corresponding to a path.
  Moves through the nodes using up the path elements.
  */
const ApocNode *apocTreeFindNode(const ApocTree *aTree, 
                                 const ApocNodePath *aPath,
                                 GError **aError)
{
    const ApocNode *retCode = NULL ;
    gchar **pathElement = NULL ;

    g_return_val_if_fail(aTree != NULL && aPath != NULL, retCode) ;
    retCode = aTree->mRoot ;
    pathElement = aPath->mPathElements ;
    if (pathElement == NULL) { return retCode ; }
    while (*pathElement)
    {
        if (retCode == NULL || apocNodeIsProperty(retCode))
        {
            return NULL ;
        }
        retCode = apocNodeFindChild((const ApocStructuralNode *) retCode, 
                                    *pathElement) ;
        ++ pathElement ;
    }
    return retCode ;
}

/**
  Lists the sub-components under a component.
  According to the mapping rules, apart from the root which is
  handled specially, there can only be one subcomponent
  */

/**
  Lists subpaths of a path.
  This entails both looking inside the tree to find the
  subnodes of the root, but also possibly finding valid
  components having the same prefix.
  */
GSList *apocTreeFindSubPaths(const ApocTree *aTree,
                             const ApocNodePath *aPath,
                             GError **aError)
{
    GSList *retCode = NULL ;
    const ApocNode *node = apocTreeFindNode(aTree, aPath, aError) ;

    if (node != NULL) 
    {
        retCode = apocNodeListSubnodes(node, retCode) ;
    }
    return apocNodePathListSubComponents(aPath, aTree->mPapi, 
                                         aTree->mConnection, 
                                         retCode, aError) ;
}

/**
  Tests for the existence of a path in the tree.
  if the path points into the tree, we check whether a node exists
  at that point, else we check whether the tree has some data or
  failing that if there are subcomponents of that tree which exist.
  */
gboolean apocTreeCheckPath(const ApocTree *aTree, 
                           const ApocNodePath *aPath,
                           GError **aError)
{
    const ApocNode *node = NULL ;
    
    g_return_val_if_fail(aTree != NULL && aPath != NULL, FALSE) ;
    if (apocNodePathIsSpecial(aPath))
    {
        return apocNodePathHasSubComponents(aPath, aTree->mPapi,
                                            aTree->mConnection, aError) ;
    }
    node = apocTreeFindNode(aTree, aPath, aError) ;
    return node != NULL  && !apocNodeIsProperty(node) ;
}

/**
  Destructor of a tree.
  Recursively destroys its nodes.
  */
void destroyApocTree(ApocTree *aTree) 
{
    if (aTree != NULL) 
    {
        destroyApocNode(aTree->mRoot, TRUE) ;
        g_free(aTree) ;
    }
}

