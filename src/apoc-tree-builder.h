/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either
 * the GNU General Public License Version 2 only ("GPL") or
 * the Common Development and Distribution License("CDDL")
 * (collectively, the "License"). You may not use this file
 * except in compliance with the License. You can obtain a copy
 * of the License at www.sun.com/CDDL or at COPYRIGHT. See the
 * License for the specific language governing permissions and
 * limitations under the License. When distributing the software,
 * include this License Header Notice in each file and include
 * the License file at /legal/license.txt. If applicable, add the
 * following below the License Header, with the fields enclosed
 * by brackets [] replaced by your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by
 * only the CDDL or only the GPL Version 2, indicate your
 * decision by adding "[Contributor] elects to include this
 * software in this distribution under the [CDDL or GPL
 * Version 2] license." If you don't indicate a single choice
 * of license, a recipient has the option to distribute your
 * version of this file under either the CDDL, the GPL Version
 * 2 or to extend the choice of license to its licensees as
 * provided above. However, if you add GPL Version 2 code and
 * therefore, elected the GPL Version 2 license, then the
 * option applies only if the new code is made subject to such
 * option by the copyright holder.
 */


#ifndef Apoc_Tree_Builder_H
#define Apoc_Tree_Builder_H

/**
  Definitions and exports only needed for tree building.
  */

/** Structural subtype of ApocNode */
typedef struct privApocStructuralNode ApocStructuralNode ;

/** Constructor */
ApocStructuralNode *createApocStructuralNode(const gchar *aName, 
                                             gboolean aIsFinalised,
                                             GError **aError) ;

/** Adds a child to the node. */
void apocNodeAddChild(ApocStructuralNode *aNode, ApocNode *aChild, 
                      GError **aError) ;

/** Possible value types */
typedef enum
{
    valueType_Unknown = 0x00,
    valueType_Boolean = 0x01,
    valueType_Short = 0x02,
    valueType_Int = 0x03,
    valueType_Long = 0x04,
    valueType_Double = 0x05,
    valueType_String = 0x06,
    valueType_HexBinary = 0x07,
    valueType_ListMask = 0x10,
    valueType_BooleanList = valueType_ListMask | valueType_Boolean,
    valueType_ShortList = valueType_ListMask | valueType_Short,
    valueType_IntList = valueType_ListMask | valueType_Int,
    valueType_LongList = valueType_ListMask | valueType_Long,
    valueType_DoubleList = valueType_ListMask | valueType_Double,
    valueType_StringList = valueType_ListMask | valueType_String,
    valueType_HexBinList = valueType_ListMask | valueType_HexBinary
} ApocValueType ;

/** Property subtype of ApocNode */
typedef struct privApocPropertyNode ApocPropertyNode ;

/** Constructor */
ApocPropertyNode *createApocPropertyNode(const gchar *aName,
                                         gboolean aIsFinalised,
                                         ApocValueType aType,
                                         GError **aError) ;

/** Adds a value. */
void apocNodeAddValue(ApocPropertyNode *aNode, const gchar *aLocale,
                      const gchar *aSeparator, gchar *aValue,
                      GError **aError) ;

/** Finds a child node by a certain name. */
const ApocNode *apocNodeFindChild(const ApocStructuralNode *aNode, 
                                  const gchar *aChildName) ;

/**
  Makes the node a root one and sets its tree to the proper value.
  */
void apocNodeSetTree(ApocNode *aNode, ApocTree *aTree) ;

/** Removes a child node and destroys it (and its descendants). */
void apocNodeDestroyChild(ApocStructuralNode *aNode, const ApocNode *aChild) ;

#endif /* Apoc_Tree_Builder_H */

