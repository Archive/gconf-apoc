/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either
 * the GNU General Public License Version 2 only ("GPL") or
 * the Common Development and Distribution License("CDDL")
 * (collectively, the "License"). You may not use this file
 * except in compliance with the License. You can obtain a copy
 * of the License at www.sun.com/CDDL or at COPYRIGHT. See the
 * License for the specific language governing permissions and
 * limitations under the License. When distributing the software,
 * include this License Header Notice in each file and include
 * the License file at /legal/license.txt. If applicable, add the
 * following below the License Header, with the fields enclosed
 * by brackets [] replaced by your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by
 * only the CDDL or only the GPL Version 2, indicate your
 * decision by adding "[Contributor] elects to include this
 * software in this distribution under the [CDDL or GPL
 * Version 2] license." If you don't indicate a single choice
 * of license, a recipient has the option to distribute your
 * version of this file under either the CDDL, the GPL Version
 * 2 or to extend the choice of license to its licensees as
 * provided above. However, if you add GPL Version 2 code and
 * therefore, elected the GPL Version 2 license, then the
 * option applies only if the new code is made subject to such
 * option by the copyright holder.
 */


#include "apoc-papi.h"
#include "common-utils.h"

#include <gmodule.h>

static const gchar *MODULE = "[ApocPapi]" ;

/** 
  Actual container of the entry points list definition.
  */
typedef struct 
{
    ApocPapi mApi ; /** Fake inheritance */
    gint mUseCount ; /** Usage count */
    GModule *mModule ; /** Dynamically-loaded module */
} ApocPapiContainer ;

static ApocPapiContainer *kPapiContainer = NULL ;

/**
  Utility method to get an error message corresponding to the
  error codes of the PAPI.
  */
const gchar *apocPapiGetError(PAPIStatus aStatus)
{
    switch (aStatus)
    {
        case PAPISuccess:
            return _("PAPI Success.") ;
        case PAPIConnectionFailure:
            return _("PAPI Connection Failure.") ;
        case PAPIAuthenticationFailure:
            return _("PAPI Authentication Failure.") ;
        case PAPINoSuchComponentFailure:
            return _("PAPI No Such Component Failure.") ;
        case PAPILDBFailure:
            return _("PAPI Database Error.") ;
        case PAPIInvalidArg:
            return _("PAPI Invalid Argument.") ;
        case PAPIOutOfMemoryFailure:
            return _("PAPI Out Of Memory.") ;
        case PAPIUnknownFailure:
        default:
            return _("Generic PAPI error.") ;
    } ;
}

/**
  Constructor of the entry points list.
  Returns a singleton with usage count. The PAPI shared object
  is loaded on creation and closed when all users of the list
  have destroyed it.
  */
ApocPapi *createApocPapi(GError **aError)
{
    if (kPapiContainer == NULL)
    {   /* Need to load the module. */
        gchar *modulePath = NULL ;
        ApocPapi *retCode = NULL ;

        g_return_val_if_fail(g_module_supported(), 
                             (ApocPapi *) kPapiContainer) ;
        modulePath = g_module_build_path(NULL, "apoc") ;
        kPapiContainer = g_new0(ApocPapiContainer, 1) ;
        kPapiContainer->mModule = g_module_open(modulePath, 0) ;
        g_free(modulePath) ;
        if (kPapiContainer->mModule == NULL)
        { 
            gconf_set_error(aError, GCONF_ERROR_FAILED,
                            _("%s Impossible to load PAPI module: %s"),
                            MODULE, g_module_error()) ;
            g_free(kPapiContainer) ;
            kPapiContainer = NULL ;
            return retCode ;
        }
        ++ kPapiContainer->mUseCount ;
        retCode = (ApocPapi *) kPapiContainer ;
        /* Load all the symbols. */
        if (
                g_module_symbol(kPapiContainer->mModule, 
                    "papiInitialise", 
                    (gpointer *) &(retCode->mInitialise)) &&
                g_module_symbol(kPapiContainer->mModule, 
                    "papiDeinitialise", 
                    (gpointer *) &(retCode->mDeinitialise)) &&
                g_module_symbol(kPapiContainer->mModule, 
                    "papiReadComponentLayers", 
                    (gpointer *) &(retCode->mReadComponentLayers)) &&
                g_module_symbol(kPapiContainer->mModule, 
                    "papiListComponentNames", 
                    (gpointer *) &(retCode->mListComponentNames)) &&
                g_module_symbol(kPapiContainer->mModule, 
                    "papiSetOffline", 
                    (gpointer *) &(retCode->mSetOffline)) &&
                g_module_symbol(kPapiContainer->mModule, 
                    "papiIsOffline", 
                    (gpointer *) &(retCode->mIsOffline)) &&
                g_module_symbol(kPapiContainer->mModule, 
                    "papiAddListener", 
                    (gpointer *) &(retCode->mAddListener)) &&
                g_module_symbol(kPapiContainer->mModule, 
                    "papiRemoveListener", 
                    (gpointer *) &(retCode->mRemoveListener)) &&
                g_module_symbol(kPapiContainer->mModule, 
                    "papiFreeLayerList", 
                    (gpointer *) &(retCode->mFreeLayerList)) &&
                g_module_symbol(kPapiContainer->mModule, 
                    "papiFreeStringList", 
                    (gpointer *) &(retCode->mFreeStringList)))
        {   /* All the mandatory symbols loaded, now we try and get the
               optional ones before returning the table. */
            g_module_symbol(kPapiContainer->mModule,
                    "papiInitialiseWithListener",
                    (gpointer *)&(retCode->mInitialiseWithListener)) ;
            return retCode ;
        }
        /* An error occurred on one of the symbols,
           let's call the whole thing off. */
        gconf_set_error(aError, GCONF_ERROR_FAILED, 
                        _("%s One or more symbols in PAPI cannot "
                          "be loaded: %s"),
                        MODULE, g_module_error()) ;
        destroyApocPapi(retCode) ;
    }
    else
    {
        ++ kPapiContainer->mUseCount ;
    }
    return (ApocPapi *) kPapiContainer ;
}

void destroyApocPapi(ApocPapi *aPapi)
{
    g_return_if_fail(aPapi == (ApocPapi *) kPapiContainer) ;
    if (aPapi != NULL &&
            -- kPapiContainer->mUseCount == 0)
    {
        /* The PAPI module launches threads which keep running even if the
           papiInitialise has failed. Unloading the module in that situation
           seems to sometimes succeed and pull the code from under the threads
           feet, resulting in a SEGV. So the module is not unloaded.
           See bug 5043774. */
        g_free(kPapiContainer) ;
        kPapiContainer = NULL ;
    }
}

ApocPapiConnection *createApocPapiConnection(ApocPapi *aPapi,
                                             PAPIConnectionListener aListener,
                                             void *aCallbackContext,
                                             GError **aError)
{
    ApocPapiConnection *retCode = NULL ;
    PAPIStatus status = PAPISuccess ;

    g_return_val_if_fail(aPapi != NULL, retCode) ;
    retCode = g_new0(ApocPapiConnection, 1) ;
    if (aListener != NULL && aPapi->mInitialiseWithListener != NULL)
    {
        retCode->mContext = aPapi->mInitialiseWithListener(NULL, 
                                                           aListener,
                                                           aCallbackContext, 
                                                           &status) ;
    }
    else 
    {
        retCode->mContext = aPapi->mInitialise(NULL, &status) ;
    }
    if (retCode->mContext == NULL || 
            (status != PAPISuccess && status != PAPIConnectionFailure))
    {
        gconf_set_error(aError, GCONF_ERROR_FAILED,
                        _("%s Cannot initialise PAPI: %s"),
                        MODULE, apocPapiGetError(status)) ;
        destroyApocPapiConnection(aPapi, retCode) ;
        return NULL ;
    }
    if (status == PAPIConnectionFailure) 
    {
        gconf_log(GCL_DEBUG, 
                  "%s PAPI Initialised in disconnected mode.", MODULE) ;
        retCode->mDisconnected = TRUE ;
    }
    return retCode ;
}
        
void destroyApocPapiConnection(ApocPapi *aPapi, ApocPapiConnection *aConnection)
{
    if (aConnection != NULL)
    {
        if (aPapi != NULL &&
                aConnection->mContext != NULL && !aConnection->mDisconnected)
        {
            aPapi->mDeinitialise(aConnection->mContext, NULL) ;
        }
        g_free(aConnection) ;
    }
}

