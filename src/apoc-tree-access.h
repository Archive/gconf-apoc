/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either
 * the GNU General Public License Version 2 only ("GPL") or
 * the Common Development and Distribution License("CDDL")
 * (collectively, the "License"). You may not use this file
 * except in compliance with the License. You can obtain a copy
 * of the License at www.sun.com/CDDL or at COPYRIGHT. See the
 * License for the specific language governing permissions and
 * limitations under the License. When distributing the software,
 * include this License Header Notice in each file and include
 * the License file at /legal/license.txt. If applicable, add the
 * following below the License Header, with the fields enclosed
 * by brackets [] replaced by your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by
 * only the CDDL or only the GPL Version 2, indicate your
 * decision by adding "[Contributor] elects to include this
 * software in this distribution under the [CDDL or GPL
 * Version 2] license." If you don't indicate a single choice
 * of license, a recipient has the option to distribute your
 * version of this file under either the CDDL, the GPL Version
 * 2 or to extend the choice of license to its licensees as
 * provided above. However, if you add GPL Version 2 code and
 * therefore, elected the GPL Version 2 license, then the
 * option applies only if the new code is made subject to such
 * option by the copyright holder.
 */


#ifndef Apoc_Tree_Access_H
#define Apoc_Tree_Access_H

#include "apoc-tree.h"

/**
  Opaque object allowing access to the configuration trees.
  */
typedef struct privApocTreeAccess ApocTreeAccess ;

/**
  Provides an instance of an Apoc tree access object.
  */
ApocTreeAccess *createApocTreeAccess(const gchar *aParameters, 
                                     GError **aError) ;

/**
  Destroys an instance of an Apoc tree access object.
  */
void destroyApocTreeAccess(ApocTreeAccess *aTreeAccess) ;

/**
  Retrieves a tree for a given component.
  The tree needs to be released with the next method when
  it's not needed any longer.
  */
const ApocTree *apocTreeAccessGetTree(const ApocTreeAccess *aTreeAccess, 
                                      const ApocNodePath *aPath,
                                      GError **aError) ;

/**
  Releases a tree obtained via apocTreeAccessGetTree.
  */
void apocTreeAccessReleaseTree(const ApocTreeAccess *aTreeAccess,
                               const ApocTree *aTree) ;

/** Callback function type for change notification. */
typedef void (*ApocTreeChangeCallback)(const ApocTree *aOldTree,
                                       const ApocTree *aNewTree,
                                       gpointer aUserData) ;

/**
  Adds a change detection notification for a tree.
  */
guint apocTreeAccessAddListener(ApocTreeAccess *aTreeAccess,
                                const ApocNodePath *aPath,
                                ApocTreeChangeCallback aCallback,
                                gpointer aUserData,
                                GError **aError) ;

/**
  Removes a change detection notification added by the previous function.
  */
void apocTreeAccessRemoveListener(ApocTreeAccess *aTreeAccess,
                                  guint aNotification) ;

/**
  Lists the tree names containing data for a node path.
  */
GSList *apocTreeAccessListTrees(const ApocTreeAccess *aTreeAccess,
                                const ApocNodePath *aPath,
                                GError **aError) ;


#endif /* Apoc_Tree_Access_H */

