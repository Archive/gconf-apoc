/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either
 * the GNU General Public License Version 2 only ("GPL") or
 * the Common Development and Distribution License("CDDL")
 * (collectively, the "License"). You may not use this file
 * except in compliance with the License. You can obtain a copy
 * of the License at www.sun.com/CDDL or at COPYRIGHT. See the
 * License for the specific language governing permissions and
 * limitations under the License. When distributing the software,
 * include this License Header Notice in each file and include
 * the License file at /legal/license.txt. If applicable, add the
 * following below the License Header, with the fields enclosed
 * by brackets [] replaced by your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by
 * only the CDDL or only the GPL Version 2, indicate your
 * decision by adding "[Contributor] elects to include this
 * software in this distribution under the [CDDL or GPL
 * Version 2] license." If you don't indicate a single choice
 * of license, a recipient has the option to distribute your
 * version of this file under either the CDDL, the GPL Version
 * 2 or to extend the choice of license to its licensees as
 * provided above. However, if you add GPL Version 2 code and
 * therefore, elected the GPL Version 2 license, then the
 * option applies only if the new code is made subject to such
 * option by the copyright holder.
 */


#ifndef ApocTree_H
#define ApocTree_H

#include "apoc-papi.h"
#include "apoc-node.h"

#include <gconf/gconf.h>

/**
  Structure and methods associated to a node path used to identify
  a location in the configuration tree.
  */
typedef struct privApocNodePath ApocNodePath ;
/**
  Accessor to the common prefix of all node paths.
  */
const gchar *apocNodePathGetCommonPrefix(void) ;
/**
  Accessor to the component part of the node path.
  */
const gchar *apocNodePathGetComponent(const ApocNodePath *aPath) ;
/**
  Builds a node path from a GConf key.
  aIsDirectory indicates how to handle the last part of the key,
  i.e as a subdirectory (TRUE) or a setting (FALSE).
  */
ApocNodePath *gconfKeyToApocNodePath(const gchar *aKey, gboolean aIsDirectory,
                                     GError **aError) ;
/**
  Returns the root of a node path, i.e one that points directly
  at the component.
  */
ApocNodePath *apocNodePathGetRoot(const ApocNodePath *aNodePath) ;
/**
  Indicates whether a node path is special, i.e could have multiple
  trees holding data for it.
  */
gboolean apocNodePathIsSpecial(const ApocNodePath *aPath) ;
/**
  Tests whether a tree name contains data for a given node path.
  */
gboolean apocNodePathIsTree(const gchar *aPath,
                            const gchar *aTree) ;
/**
  Returns a path which is the common part between two other paths,
  that is one which contains both argument paths.
  The path returned may be one of the arguments if it contains the other one.
  */
ApocNodePath *apocNodePathGetCommonPath(const ApocNodePath *aPath1,
                                        const ApocNodePath *aPath2) ;
/**
  Returns a node path representing the start of the replacement nodes,
  i.e the properties whose names can be used as placeholders in other trees'
  properties.
  */
ApocNodePath *apocNodePathGetReplacementPath(void) ;
/**
  Destroys a node path created by one of the gconfXxxToApocNodePath.
  */
void destroyApocNodePath(ApocNodePath *aPath) ;


/**
  Structure and methods associated to a data tree, i.e the merged
  configuration data for a component.
  */
typedef struct privApocTree ApocTree ;
/**
  Constructor from a node path.
  */
ApocTree *createApocTree(const ApocNodePath *aPath, 
                         const ApocPapi *aPapi,
                         const ApocPapiConnection *aConnection,
                         GError **aError) ;
/**
  Constructor from another tree (cloning).
  Only the characteristics of the tree are copied, not its data.
  */
ApocTree *cloneApocTree(const ApocTree *aTree, GError **aError) ;
/**
  Returns the name (i.e the component) of a tree.
  */
const gchar *apocTreeGetName(const ApocTree *aTree) ;
/**
  Returns the last modification time of a tree. 
  */
GTime apocTreeGetModificationTime(const ApocTree *aTree) ;
/**
  Returns the stub of GConf path corresponding to the root of a tree.
  */
GString *apocTreeGetPath(const ApocTree *aTree) ;
/**
  Returns a reference to the node associated to a path.
  The component specified in the node path is ignored (assumed to
  be the same as the tree's). A null node is returned if no node
  exists for that path in that tree.
  Errors are only returned in case of problems accessing the tree itself.
  */
const ApocNode *apocTreeFindNode(const ApocTree *aTree,
                                 const ApocNodePath *aPath,
                                 GError **aError) ;
/**
  Returns the list of the valid subpaths for a given one.
  */
GSList *apocTreeFindSubPaths(const ApocTree *aTree, 
                             const ApocNodePath *aPath,
                             GError **aError) ;
/**
  Checks the existence of a path in the tree.
  */
gboolean apocTreeCheckPath(const ApocTree *aTree, 
                           const ApocNodePath *aPath,
                           GError **aError) ;
/**
  Destructor of a data tree.
  */
void destroyApocTree(ApocTree *aTree) ;


#endif /* ApocTree_H */

