#
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
# 
# Copyright 2007 Sun Microsystems, Inc. All rights reserved.
# 
# The contents of this file are subject to the terms of either
# the GNU General Public License Version 2 only ("GPL") or
# the Common Development and Distribution License("CDDL")
# (collectively, the "License"). You may not use this file
# except in compliance with the License. You can obtain a copy
# of the License at www.sun.com/CDDL or at COPYRIGHT. See the
# License for the specific language governing permissions and
# limitations under the License. When distributing the software,
# include this License Header Notice in each file and include
# the License file at /legal/license.txt. If applicable, add the
# following below the License Header, with the fields enclosed
# by brackets [] replaced by your own identifying information:
# "Portions Copyrighted [year] [name of copyright owner]"
# 
# Contributor(s):
# 
# If you wish your version of this file to be governed by
# only the CDDL or only the GPL Version 2, indicate your
# decision by adding "[Contributor] elects to include this
# software in this distribution under the [CDDL or GPL
# Version 2] license." If you don't indicate a single choice
# of license, a recipient has the option to distribute your
# version of this file under either the CDDL, the GPL Version
# 2 or to extend the choice of license to its licensees as
# provided above. However, if you add GPL Version 2 code and
# therefore, elected the GPL Version 2 license, then the
# option applies only if the new code is made subject to such
# option by the copyright holder.
#

msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2004-01-06 13:05+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/apoc-backend.c:96
#, c-format
msgid "%s Unsupported operation."
msgstr ""

#: src/apoc-node.c:214
#, c-format
msgid "%s Trying to access an empty property node."
msgstr ""

#: src/apoc-node.c:247
#, c-format
msgid "%s Invalid value type (%d)."
msgstr ""

#: src/apoc-node.c:288
#, c-format
msgid "%s Error creating value from string: %s"
msgstr ""

#: src/apoc-node.c:478
#, c-format
msgid "%s Trying to turn a structural node %s into a value."
msgstr ""

#: src/apoc-node.c:550
#, c-format
msgid "%s Trying to list properties of property node %s."
msgstr ""

#: src/apoc-node.c:676
#, c-format
msgid "%s Trying to list subnodes of property node %s."
msgstr ""

#: src/apoc-node.c:956
#, c-format
msgid "%s Node %s changed its type over time!"
msgstr ""

#: src/apoc-papi.c:29
msgid "PAPI Success."
msgstr ""

#: src/apoc-papi.c:31
msgid "PAPI Connection Failure."
msgstr ""

#: src/apoc-papi.c:33
msgid "PAPI Authentication Failure."
msgstr ""

#: src/apoc-papi.c:35
msgid "PAPI No Such Component Failure."
msgstr ""

#: src/apoc-papi.c:37
msgid "PAPI Database Error."
msgstr ""

#: src/apoc-papi.c:39
msgid "PAPI Invalid Argument."
msgstr ""

#: src/apoc-papi.c:41
msgid "PAPI Out Of Memory."
msgstr ""

#: src/apoc-papi.c:44
msgid "Generic PAPI error."
msgstr ""

#: src/apoc-papi.c:71
#, c-format
msgid "%s Impossible to load PAPI module: %s"
msgstr ""

#: src/apoc-papi.c:117
#, c-format
msgid "%s One or more symbols in PAPI cannot be loaded: %s"
msgstr ""

#: src/apoc-parser.c:514
#, c-format
msgid "%s Malformed data, operation %s unknown."
msgstr ""

#: src/apoc-parser.c:624
#, c-format
msgid "%s Malformed data, couldn't find type of %s."
msgstr ""

#: src/apoc-parser.c:836
#, c-format
msgid "%s Parsing of blob failed (%d)."
msgstr ""

#: src/apoc-source.c:77
#, c-format
msgid "%s Only 'readonly' address flag supported."
msgstr ""

#: src/apoc-source.c:109
#, c-format
msgid "%s Source %s is unavailable: %s"
msgstr ""

#: src/apoc-source.c:116
#, c-format
msgid "%s Source %s is unavailable."
msgstr ""

#: src/apoc-source.c:320
#, c-format
msgid "%s Error mapping node to value: %s"
msgstr ""

#: src/apoc-source.c:513
#, c-format
msgid "%s Couldn't register listener: %s"
msgstr ""

#: src/apoc-source.c:520
#, c-format
msgid "%s Couldn't register listener."
msgstr ""

#: src/apoc-source.c:598
#, c-format
msgid "%s Couldn't convert key '%s' to node path: %s"
msgstr ""

#: src/apoc-tree-access.c:87
#, c-format
msgid "%s Received notification for non-existent tree (%s)."
msgstr ""

#: src/apoc-tree-access.c:188
#, c-format
msgid "%s Cannot initialise PAPI: %s"
msgstr ""

#: src/apoc-tree-access.c:288
#, c-format
msgid "%s Cannot add listener for %s to PAPI: %s"
msgstr ""

#: src/apoc-tree-access.c:463
#, c-format
msgid "%s Error registering listener for %s: %s"
msgstr ""

#: src/apoc-tree-access.c:545
#, c-format
msgid "%s Trying to remove an inexistent listener."
msgstr ""

#: src/apoc-tree-access.c:552
#, c-format
msgid "%s Notification has no associated tree."
msgstr ""

#: src/apoc-tree.c:360
#, c-format
msgid "%s Error listing component names: %s"
msgstr ""

#: src/apoc-tree.c:507
#, c-format
msgid "%s Error reading layers for %s: %s"
msgstr ""
